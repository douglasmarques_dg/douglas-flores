package com.pethop.service;

import com.petshop.DAO.ClienteDAO;
import com.petshop.entity.Cliente;

public class ClienteService extends GenericService< Cliente, Long, ClienteDAO > {
    
    private static final ClienteService INSTANCE;
    
    static {
        INSTANCE = new ClienteService();
    }
    
    public static ClienteService getInstance(){
        return INSTANCE;
    }
        
    @Override
    protected ClienteDAO getDao() {
        return ClienteDAO.getInstance();
    }
}