import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest {
    @Test
    public void popularAgendaTest() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        agenda.adicionar("Mithrandir", "444444");
        agenda.adicionar("teste", "teste");
        assertEquals("555555", agenda.consultar("Bernardo"));
        assertEquals("444444", agenda.consultar("Mithrandir"));
        String contatos = String.format("Bernardo,555555%sMithrandir,444444%steste,teste%s", System.lineSeparator(),System.lineSeparator(),System.lineSeparator());
        assertEquals(contatos, agenda.csv());
    }
}