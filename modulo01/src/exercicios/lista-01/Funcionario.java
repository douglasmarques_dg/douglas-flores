public class Funcionario {
    String nome;
    double salarioFixo, totalVendasMes;
    
    public Funcionario(String nome, double salarioFixo, double totalVendasMes) {
        this.nome = nome;
        this.salarioFixo = salarioFixo;
        this.totalVendasMes = totalVendasMes;
    }
    
    //Exercício 2
    public double getLucro() {
        return ((this.salarioFixo * 0.9) + (totalVendasMes * 0.15) * 0.9);
    }
}
