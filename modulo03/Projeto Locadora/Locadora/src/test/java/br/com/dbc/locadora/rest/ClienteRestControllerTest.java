package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.CepDTO;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author douglas.marques
 */
public class ClienteRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private ClienteRestController clienteRestController;

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    protected AbstractRestController getController() {
        return clienteRestController;
    }

    @Before
    public void beforeTest() {
        clienteRepository.deleteAll();
    }

    @After
    public void tearDown() {

    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void createClienteTest() throws Exception {
        Cliente c = Cliente.builder()
                .nome("nome")
                .telefone("9999")
                .rua("Rua")
                .bairro("Bairro")
                .cidade("Cidade")
                .estado("Estado")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(c.getEstado()));
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c.getRua(), clientes.get(0).getRua());
        Assert.assertEquals(c.getCidade(), clientes.get(0).getCidade());
        Assert.assertEquals(c.getBairro(), clientes.get(0).getBairro());
        Assert.assertEquals(c.getEstado(), clientes.get(0).getEstado());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void buscarNomeTest() throws Exception {
        Cliente c = Cliente.builder()
                .nome("nome")
                .telefone("9999")
                .rua("Rua")
                .bairro("Bairro")
                .cidade("Cidade")
                .estado("Estado")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(c.getEstado()));

        restMockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/cliente/buscar/nome/%s", c.getNome()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].estado").value(c.getEstado()));
    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void buscarTelefoneTest() throws Exception {
        Cliente c = Cliente.builder()
                .nome("nome")
                .telefone("9999")
                .rua("Rua")
                .bairro("Bairro")
                .cidade("Cidade")
                .estado("Estado")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(c.getEstado()));

        restMockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/cliente/buscar/telefone/%s", c.getTelefone()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].estado").value(c.getEstado()));
    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void buscarEnderecoTest() throws Exception {
        Cliente c = Cliente.builder()
                .nome("nome")
                .telefone("9999")
                .rua("Rua")
                .bairro("Bairro")
                .cidade("Cidade")
                .estado("Estado")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(c.getEstado()));

        restMockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/cliente/buscar/endereco/%s", c.getBairro()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].estado").value(c.getEstado()));;
    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void buscarCEPTest() throws Exception {
        CepDTO cep = objectMapper.readValue(restMockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/cliente/cep/94410560"))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn().getResponse().getContentAsString(), CepDTO.class);
        Assert.assertEquals("Rua Caruaru", cep.getRua());
        Assert.assertEquals("Cocão", cep.getBairro());
        Assert.assertEquals("Viamão", cep.getCidade());
        Assert.assertEquals("RS", cep.getEstado());
    }
}
