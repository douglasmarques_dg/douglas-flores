package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author douglas.marques
 */
public class ClienteDAOHIBERNATE {

    private static ClienteDAOHIBERNATE instance;
    protected SessionFactory sessionFactory;

    public static ClienteDAOHIBERNATE getInstance() {
        if (instance == null) {
            instance = new ClienteDAOHIBERNATE();
        }
        return instance;
    }

    public ClienteDAOHIBERNATE() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public List<Cliente> findAll() {
        Session session = sessionFactory.openSession();
        return session.createCriteria(Cliente.class).list();
    }

    public List<Cliente> findName(String nome) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            return session.createCriteria(Cliente.class)
                    .add(Restrictions.ilike("nome", nome))
                    .list();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial Session creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        } finally {
            session.close();
        }
    }

    public void saveOrUpdate(Cliente cliente) {
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();
        try {
            session.saveOrUpdate(cliente);
            t.commit();
        } catch (Exception e) {
            t.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    public void delete(Cliente cliente) {
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();
        try {
            session.delete(cliente);
            t.commit();
        } catch (Exception e) {
            t.rollback();
            throw e;
        } finally {
            session.close();
        }
    }
}
