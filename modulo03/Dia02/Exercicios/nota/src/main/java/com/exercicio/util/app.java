package com.exercicio.util;

import com.exercicio.DAO.ProdutoDAO;
import com.exercicio.model.Produto;

/**
 *
 * @author douglas.marques
 */
public class app {
    private static final ProdutoDAO produtoDAO = new ProdutoDAO();
    public static void main(String... args) {
        Produto teste = new Produto(null, "Produto de teste");
        produtoDAO.persistir(teste);
    }
}
