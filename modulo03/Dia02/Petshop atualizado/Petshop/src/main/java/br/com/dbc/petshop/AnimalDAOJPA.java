package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.PersistenceUtils;
import java.util.List;
import javax.persistence.EntityManager;

public class AnimalDAOJPA {

    private static AnimalDAOJPA instance;
    protected EntityManager entityManager;

    public static AnimalDAOJPA getInstance() {
        if (instance == null) {
            instance = new AnimalDAOJPA();
        }
        return instance;
    }

    public AnimalDAOJPA() {
        entityManager = PersistenceUtils.getEm();
    }

    public Animal getById(final int id) {
        return entityManager.find(Animal.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Animal> findAll() {
        return entityManager.createQuery("FROM " + Animal.class.getName()).getResultList();
    }

    public void persistir(Animal animal) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(animal);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Animal animal) {
        try {
            entityManager.getTransaction().begin();
            if (animal.getId() == null) {
                entityManager.persist(animal);
            } else {
                animal = entityManager.merge(animal);
            }
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remover(Animal animal) {
        try {
            entityManager.getTransaction().begin();
            animal = entityManager.find(Animal.class, animal.getId());
            entityManager.remove(animal);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id) {
        try {
            Animal animal = getById(id);
            remover(animal);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
