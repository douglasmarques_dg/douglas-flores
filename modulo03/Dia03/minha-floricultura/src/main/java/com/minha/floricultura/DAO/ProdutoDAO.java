package com.minha.floricultura.DAO;

import com.minha.floricultura.entity.Produto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author douglas.marques
 */
@Stateless
public class ProdutoDAO extends AbstractDAO<Produto> {

    @PersistenceContext(unitName = "minha-floricultura-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProdutoDAO() {
        super(Produto.class);
    }
    
    public List<Produto> findDescricao(String des) {
        return em.createNativeQuery(String.format("SELECT * FROM Produto pr WHERE descricao = '%s'", des), Produto.class).getResultList();
    }
}
