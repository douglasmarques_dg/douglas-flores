package com.mycompany.localizacao.DAO;

import com.mycompany.localizacao.model.Pais;
import com.mycompany.localizacao.util.PersistenceUtil;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author douglas.marques
 */
public class PaisDAOTest {

    public PaisDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        PersistenceUtil.beginTransaction();
        PersistenceUtil.getEm().createNativeQuery("delete from pais").executeUpdate();
        PersistenceUtil.getEm().getTransaction().commit();
    }

    /**
     * Test of getById method, of class PaisDAO.
     */
    @Test
    public void testGetById() {
        System.out.println("getById");

        PaisDAO instance = new PaisDAO();
        long id = 3;

        Pais pais = new Pais(null, "Congo", "Con");
        instance.persistir(pais);

        Pais result = instance.getById(id);
        assertEquals("Congo", result.getNamepais());
    }

    /**
     * Test of findAll method, of class PaisDAO.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");

        PaisDAO instance = new PaisDAO();

        List<Pais> result = instance.findAll();
        assertEquals(0, result.size());

        Pais pais = new Pais(null, "Nicaragua", "Nic");
        instance.persistir(pais);

        result = instance.findAll();
        assertEquals(1, result.size());
    }

    /**
     * Test of persistir method, of class PaisDAO.
     */
    @Test
    public void testPersistir() {
        System.out.println("persistir");

        PaisDAO instance = new PaisDAO();
        Pais pais = new Pais(null, "Venezuela", "Ven");

        instance.persistir(pais);

        List<Pais> paises = instance.findAll();
        assertEquals(1, paises.size());
        assertEquals("Venezuela", paises.get(0).getNamepais());
    }

    /**
     * Test of merge method, of class PaisDAO.
     */
    @Test
    public void testMerge() {
        System.out.println("merge");

        PaisDAO instance = new PaisDAO();

        Pais pais = new Pais(null, "Estados Unidos", "EUA");
        instance.merge(pais);

        List<Pais> paises = instance.findAll();
        assertEquals(1, paises.size());
        assertEquals("Estados Unidos", paises.get(0).getNamepais());

        pais.setNamepais("United States");
        instance.merge(pais);

        paises = instance.findAll();
        assertEquals(1, paises.size());
        assertEquals("United States", paises.get(0).getNamepais());
    }

    /**
     * Test of remover method, of class PaisDAO.
     */
    @Test
    public void testRemover() {
        System.out.println("remover");

        PaisDAO instance = new PaisDAO();

        Pais pais = new Pais(null, "Portugal", "Por");
        instance.persistir(pais);

        List<Pais> paises = instance.findAll();
        assertEquals(1, paises.size());
        assertEquals("Portugal", paises.get(0).getNamepais());

        instance.remover(pais);

        paises = instance.findAll();
        assertEquals(0, paises.size());
    }

    /**
     * Test of removeById method, of class PaisDAO.
     */
    @Test
    public void testRemoveById() {
        System.out.println("removeById");

        PaisDAO instance = new PaisDAO();

        Pais pais = new Pais(null, "Argentina", "Arg");
        instance.persistir(pais);

        List<Pais> paises = instance.findAll();
        assertEquals(1, paises.size());
        assertEquals("Argentina", paises.get(0).getNamepais());

        instance.removeById(pais.getIdpais());

        paises = instance.findAll();
        System.out.println(paises);
        assertEquals(0, paises.size());
    }
}
