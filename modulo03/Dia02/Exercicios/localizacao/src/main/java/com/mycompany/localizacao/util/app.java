package com.mycompany.localizacao.util;

import com.mycompany.localizacao.DAO.PaisDAO;
import com.mycompany.localizacao.model.Pais;

/**
 *
 * @author douglas.marques
 */
public class app {
    private static final PaisDAO paisDao = new PaisDAO();
    
    public static void main(String... args) {
        Pais teste = new Pais(null, "Brasil", "HUE");
        paisDao.persistir(teste);
    }
}
