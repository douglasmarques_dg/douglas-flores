import java.util.*;

public class AgendaContatos {
    private LinkedHashMap<String, String> contatos;
    
    public AgendaContatos() {
        contatos = new LinkedHashMap<>();
    }
    
    public void adicionar(String nome, String telefone) {
        if(nome == null || telefone == null) return;
        contatos.put(nome, telefone);
    }
    
    public String consultar(String nome) {
        return contatos.get(nome);
    }
    
    public String csv() {
        StringBuilder contatos = new StringBuilder();
        for(Map.Entry<String, String> entrada : this.contatos.entrySet())
            contatos.append(String.format("%s,%s%s", entrada.getKey(),entrada.getValue(), System.lineSeparator()));
        return contatos.toString();
    }
}