public class ItemImperdivel extends Item {
    public ItemImperdivel(String descricao, int quantidade) {
        super(descricao, quantidade);
        if(quantidade < 1) setQuantidade(1);
        else setQuantidade(quantidade);
    }
    
    public void setQuantidade(int novaQuantidade) {
        if(novaQuantidade < 1) return;
        super.setQuantidade(novaQuantidade);
    }
}