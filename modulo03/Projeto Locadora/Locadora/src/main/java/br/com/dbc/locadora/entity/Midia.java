package br.com.dbc.locadora.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas.marques
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Midia extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "TIPO")
    @Enumerated(EnumType.STRING)
    private MidiaType tipo;

    @JoinColumn(name = "ID_ALUGUEL", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private Aluguel idAluguel;

    @NotNull
    @JoinColumn(name = "ID_FILME", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Filme idFilme;
}
