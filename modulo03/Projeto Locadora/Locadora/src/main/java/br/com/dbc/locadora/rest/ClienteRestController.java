package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.service.ClienteService;
import br.com.dbc.locadora.service.CorreiosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author douglas.marques
 */
@PreAuthorize("hasAuthority('ADMIN_USER')")
@RestController
@RequestMapping("/api/cliente")
public class ClienteRestController extends AbstractRestController<Cliente, Long, ClienteService> {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private CorreiosService correiosService;

    @Override
    protected ClienteService getService() {
        return clienteService;
    }

    @GetMapping("/buscar/nome/{nome}")
    public ResponseEntity<?> getNome(Pageable pageable, @PathVariable String nome) {
        return ResponseEntity.ok(clienteService.findByNome(pageable, nome));
    }

    @GetMapping("/buscar/telefone/{telefone}")
    public ResponseEntity<?> getTelefone(Pageable pageable,@PathVariable String telefone) {
        return ResponseEntity.ok(clienteService.findByTelefone(pageable, telefone));
    }

    @GetMapping("/buscar/endereco/{endereco}")
    public ResponseEntity<?> getEndereco(Pageable pageable, @PathVariable String endereco) {
        return ResponseEntity.ok(clienteService.findByEndereco(pageable, endereco));
    }

    @GetMapping("/cep/{cep}")
    public ResponseEntity<?> getCep(@PathVariable String cep) {
        return ResponseEntity.ok(correiosService.buscarCEP(cep));
    }
}
