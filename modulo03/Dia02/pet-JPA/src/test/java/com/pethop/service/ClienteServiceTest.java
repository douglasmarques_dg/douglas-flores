package com.pethop.service;

import com.petshop.DAO.ClienteDAO;
import com.petshop.DAO.PersistenceUtil;
import com.petshop.entity.Animal;
import com.petshop.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Matchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.internal.util.reflection.Whitebox;

/**
 *
 * @author douglas.marques
 */
public class ClienteServiceTest {

    public ClienteServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
       PersistenceUtil.beginTransaction();
       PersistenceUtil.getEm().createQuery("delete from Cliente").executeUpdate();
       PersistenceUtil.getEm().getTransaction().commit();
    }
    
    @Test
    public void findAllTest() {
        System.out.println("Buscar todos os clientes");
        ClienteService instanceCliente = new ClienteService();
        List<Cliente> resultado = instanceCliente.findAll();
        assertEquals(0, resultado.size());
    }
    
   //@Test
   public void testFindOneMocked() {
       ClienteService clienteService = new ClienteService();
       ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
       Whitebox.setInternalState(clienteService, "INSTANCE", clienteDAO);
       Cliente created = Cliente.builder()
               .id(1l)
               .nomecliente("Jovem")
               .animalList(Arrays
               .asList(Animal.builder()
                       .nomeanimal("Velho")
                       .build()))
               .build();
       Mockito.doReturn(created).when(clienteDAO).findOne(created.getId());
       Cliente returned = clienteService.findOne(created.getId());
       assertEquals(created.getId(), returned.getId());
       Mockito.verify(clienteDAO, times(1)).findOne(created.getId());
   }
   
    @Test
    public void testCreateMocked() {
        System.out.println("Criar Cliente com Mock");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).create(any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);
        Cliente cliente = Cliente.builder()
                .nomecliente("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nomeanimal("Animal 1")
                                .build()))
                .build();
        clienteService.create(cliente);
        verify(daoMock, times(1)).create(any());
    }

    @Test
    public void createTest() {
        System.out.println("Criar cliente");
        ClienteService instanceCliente = new ClienteService();
        Cliente cliente = Cliente.builder()                
                .nomecliente("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nomeanimal("Animal 1")
                                .build()))
                .build();
        instanceCliente.create(cliente);
        List<Cliente> resultado = instanceCliente.findAll();
        assertEquals(1, resultado.size());
        assertEquals("Cliente 1", resultado.get(0).getNomecliente());  
        assertEquals(1, resultado.get(0).getAnimalList().size());  
        assertEquals("Animal 1", resultado.get(0).getAnimalList().get(0).getNomeanimal());  
    }
    
    @Test
    public void updateTest() {
        System.out.println("Criar cliente");
        ClienteService instanceCliente = new ClienteService();
        Cliente cliente = Cliente.builder()                
                .nomecliente("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nomeanimal("Animal 1")
                                .build()))
                .build();
        instanceCliente.create(cliente);
        List<Cliente> resultado = instanceCliente.findAll();
        assertEquals(1, resultado.size());    
        assertEquals("Cliente 1", resultado.get(0).getNomecliente());  
        assertEquals(1, resultado.get(0).getAnimalList().size());  
        assertEquals("Animal 1", resultado.get(0).getAnimalList().get(0).getNomeanimal());  
        cliente.setNomecliente("Cliente 2");        
        resultado = instanceCliente.findAll();
        assertEquals(1, resultado.size());
        assertEquals("Cliente 2", resultado.get(0).getNomecliente());  
        assertEquals(1, resultado.get(0).getAnimalList().size());  
        assertEquals("Animal 1", resultado.get(0).getAnimalList().get(0).getNomeanimal());  
    }
}
