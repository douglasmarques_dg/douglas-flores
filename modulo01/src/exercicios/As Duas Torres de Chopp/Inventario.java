import java.util.ArrayList;

public class Inventario {
    private ArrayList<Item> itens;

    public Inventario(int num) {
        this.itens = new ArrayList<>(num);
    }

    public ArrayList<Item> getItems() {
        return this.itens;
    }

    public void adicionarItem(Item item) {
        this.itens.add(item);
    }

    public Item obter(int pos) {
        if(pos < 0 || pos > this.itens.size() || this.itens == null || this.itens.size() == 0) return null;
        return this.itens.get(pos);
    }
    
    public Item buscarItem(String desc) {
        for(Item i : this.itens) 
            if(i.getDescricao().equalsIgnoreCase(desc)) return i;
        return null;        
    }

    public Item remover(int pos) {
        if(pos < 0 || pos > this.itens.size()) return null;
        return this.itens.remove(pos);
    }
    
    public int tamanho() {
        int tam = 0;
        for(Item i : this.itens) tam++;
        return tam;
    }
    
    public void ordenarItens() {
        if(this.itens == null || this.itens.size() == 0) return;
        for(int i = 1; i < this.itens.size(); i++)
            for(int j = 1; j <= this.itens.size()-i; j++)
                if(this.itens.get(j-1).getQuantidade() > this.itens.get(j).getQuantidade()) {
                    Item temp = this.itens.get(j-1);
                    this.itens.set(j-1, this.itens.get(j));
                    this.itens.set(j, temp);
                }
    }

    public Item primeiroItemDoInventario() {
        for(int i = 0; i < this.itens.size(); i++)
            if(this.itens.get(i) != null) return this.itens.get(i);
        return null;
    }

    public Item maiorQuantidade() {
        Item maior = primeiroItemDoInventario();
        if(maior == null) return null;
        for(int i = 0; i < this.itens.size(); i++)
            if(this.itens.get(i) != null)
                if(this.itens.get(i).getQuantidade() > maior.getQuantidade()) maior = this.itens.get(i);
        return maior;
    }
    
    public ArrayList<Item> inverter() {
        if(this.itens == null) return null;
        ArrayList<Item> itensInv = new ArrayList<>(this.itens.size());
        for(int i = this.itens.size()-1; i >= 0; i--)
            itensInv.add(this.itens.get(i));            
        return itensInv;
    }

    public String toString() {
        if(this.itens.size() == 0) return null;
        String objetos = "";
        for(int i = 0; i < this.itens.size(); i++) 
            if(this.itens.get(i) != null) {
                objetos += this.itens.get(i).getDescricao();
                if(i < this.itens.size()-1) objetos += ","; 
            }
        return objetos;
    }
}