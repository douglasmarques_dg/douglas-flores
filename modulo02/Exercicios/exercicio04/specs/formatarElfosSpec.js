describe("Formatar Elfos", function() {
  beforeEach(function() {
    chai.should();
  });

  it("Deve formatar um array com dois objetos de elfos", function() {
    const elfos = [
      { nome: "LEGOLAS", experiencia: false, qtdFlechas: 6 },
      { nome: "GALADRIEL", experiencia: true, qtdFlechas: 1 }
    ];
    const resultado = formatarElfos(elfos);
    resultado.should.eql([
      {
        nome: "LEGOLAS",
        temExperiencia: false,
        qtdFlechas: 6,
        descricao: "LEGOLAS sem experiencia e com 6 flechas"
      },
      {
        nome: "GALADRIEL",
        temExperiencia: true,
        qtdFlechas: 1,
        descricao: "GALADRIEL com experiencia e com 1 flecha"
      }
    ]);
  });

  it("Deve formatar um array com apenas um objeto de elfo", function() {
    const elfos = [{ nome: "LEGOLAS", experiencia: false, qtdFlechas: 6 }];
    const resultado = formatarElfos(elfos);
    resultado.should.eql([
      {
        nome: "LEGOLAS",
        temExperiencia: false,
        qtdFlechas: 6,
        descricao: "LEGOLAS sem experiencia e com 6 flechas"
      }
    ]);
  });

  it("Deve formatar um array com dez objetos de elfos", function() {
    const elfos = [
      { nome: "Legolas", experiencia: false, qtdFlechas: 6 },
      { nome: "Galadriel", experiencia: true, qtdFlechas: 1 },
      { nome: "Glorfindel", experiencia: true, qtdFlechas: 7 },
      { nome: "Gil-Galad", experiencia: false, qtdFlechas: 2 },
      { nome: "Fingolfin", experiencia: false, qtdFlechas: 5 },
      { nome: "Feanor", experiencia: true, qtdFlechas: 3 },
      { nome: "Luthien", experiencia: true, qtdFlechas: 4 },
      { nome: "Elrond", experiencia: true, qtdFlechas: 1 },
      { nome: "Elessar", experiencia: true, qtdFlechas: 1 },
      { nome: "Alduin", experiencia: true, qtdFlechas: 2 }
    ];
    const resultado = formatarElfos(elfos);
    resultado.should.eql([
      {
        nome: "LEGOLAS",
        temExperiencia: false,
        qtdFlechas: 6,
        descricao: "LEGOLAS sem experiencia e com 6 flechas"
      },
      {
        nome: "GALADRIEL",
        temExperiencia: true,
        qtdFlechas: 1,
        descricao: "GALADRIEL com experiencia e com 1 flecha"
      },
      {
        nome: "GLORFINDEL",
        temExperiencia: true,
        qtdFlechas: 7,
        descricao: "GLORFINDEL com experiencia e com 7 flechas"
      },
      {
        nome: "GIL-GALAD",
        temExperiencia: false,
        qtdFlechas: 2,
        descricao: "GIL-GALAD sem experiencia e com 2 flechas"
      },
      {
        nome: "FINGOLFIN",
        temExperiencia: false,
        qtdFlechas: 5,
        descricao: "FINGOLFIN sem experiencia e com 5 flechas"
      },
      {
        nome: "FEANOR",
        temExperiencia: true,
        qtdFlechas: 3,
        descricao: "FEANOR com experiencia e com 3 flechas"
      },
      {
        nome: "LUTHIEN",
        temExperiencia: true,
        qtdFlechas: 4,
        descricao: "LUTHIEN com experiencia e com 4 flechas"
      },
      {
        nome: "ELROND",
        temExperiencia: true,
        qtdFlechas: 1,
        descricao: "ELROND com experiencia e com 1 flecha"
      },
      {
        nome: "ELESSAR",
        temExperiencia: true,
        qtdFlechas: 1,
        descricao: "ELESSAR com experiencia e com 1 flecha"
      },
      {
        nome: "ALDUIN",
        temExperiencia: true,
        qtdFlechas: 2,
        descricao: "ALDUIN com experiencia e com 2 flechas"
      }
    ]);
  });
});
