package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author douglas.marques
 */
public class AnimalDAOHIBERNATETest {

    public AnimalDAOHIBERNATETest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        PersistenceUtils.beginTransaction();
        PersistenceUtils.getEm().createNativeQuery("delete from animal").executeUpdate();
        PersistenceUtils.getEm().createNativeQuery("delete from cliente").executeUpdate();
        PersistenceUtils.getEm().getTransaction().commit();
    }

    /**
     * Test of findAll method, of class AnimalDAOHIBERNATE.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");

        AnimalDAOHIBERNATE instance = new AnimalDAOHIBERNATE();

        List<Animal> result = instance.findAll();
        assertEquals(0, result.size());
    }

    /**
     * Test of findName method, of class AnimalDAOHIBERNATE.
     */
    @Test
    public void testFindName() {
        System.out.println("findName");

        String nome = "AUAU";
        AnimalDAOHIBERNATE instance = new AnimalDAOHIBERNATE();

        List<Animal> result = instance.findName(nome);
        assertEquals(0, result.size());
    }

    /**
     * Test of saveOrUpdate method, of class AnimalDAOHIBERNATE.
     */
    @Test
    public void testSaveOrUpdate() {
        System.out.println("saveOrUpdate");

        Cliente cliente = new Cliente(null, "Cliente para teste", SexoType.M, "Profissao teste");
        ClienteDAOJPA instanceCliente = new ClienteDAOJPA();
        instanceCliente.persistir(cliente);

        long preco = 999;
        Animal animal = new Animal(null, "Animal para testar", SexoType.F, cliente, preco);

        AnimalDAOHIBERNATE instance = new AnimalDAOHIBERNATE();
        instance.saveOrUpdate(animal);
        List<Animal> result = instance.findAll();
        assertEquals(1, result.size());

        animal.setNome("Animal atualizado");
        instance.saveOrUpdate(animal);
        result = instance.findAll();
        assertEquals(1, result.size());
    }

    /**
     * Test of delete method, of class AnimalDAOHIBERNATE.
     */
    @Test
    public void testDelete() {
        System.out.println("delete");

        Cliente cliente = new Cliente(null, "Cliente para teste", SexoType.M, "Profissao teste");
        ClienteDAOJPA instanceCliente = new ClienteDAOJPA();
        instanceCliente.persistir(cliente);

        long preco = 999;
        Animal animal = new Animal(null, "Animal para remover", SexoType.F, cliente, preco);

        AnimalDAOHIBERNATE instance = new AnimalDAOHIBERNATE();
        instance.saveOrUpdate(animal);

        List<Animal> result = instance.findAll();
        assertEquals(1, result.size());

        instance.delete(animal);

        result = instance.findAll();
        assertEquals(0, result.size());
    }

}
