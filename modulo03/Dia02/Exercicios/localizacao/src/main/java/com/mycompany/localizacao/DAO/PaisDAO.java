package com.mycompany.localizacao.DAO;

import com.mycompany.localizacao.model.Pais;
import com.mycompany.localizacao.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;

public class PaisDAO {

    private static PaisDAO instance;
    protected EntityManager entityManager;

    public static PaisDAO getInstance() {
        if (instance == null) {
            instance = new PaisDAO();
        }
        return instance;
    }

    public PaisDAO() {
        entityManager = PersistenceUtil.getEm();
    }

    public Pais getById(final long id) {
        return entityManager.find(Pais.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Pais> findAll() {
        return entityManager.createQuery("FROM " + Pais.class.getName()).getResultList();
    }

    public void persistir(Pais pais) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(pais);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Pais pais) {
        try {
            entityManager.getTransaction().begin();
            if (pais.getIdpais() == null) {
                entityManager.persist(pais);
            } else {
                pais = entityManager.merge(pais);
            }
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remover(Pais pais) {
        try {
            entityManager.getTransaction().begin();
            pais = entityManager.find(Pais.class, pais.getIdpais());
            entityManager.remove(pais);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final long id) {
        try {
            Pais pais = getById(id);
            remover(pais);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
