package com.minha.floricultura.DAO;

import com.minha.floricultura.entity.Cliente;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author douglas.marques
 */
@Stateless
public class ClienteDAO extends AbstractDAO<Cliente> {

    @PersistenceContext(unitName = "minha-floricultura-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClienteDAO() {
        super(Cliente.class);
    }
}
