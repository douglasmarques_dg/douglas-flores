// Exercício-01
function calcularCirculo({ raio, tipoCalculo }) {
  if (tipoCalculo === "A") return Math.PI * raio ** 2;
  if (tipoCalculo === "C") return Math.PI * 2 * raio;
}

// Exercício-02
function naoBissexto(ano) {
  return !(ano % 4 === 0 || ano % 400 === 0);
}

// Exercício-03
function concatenarSemUndefined(texto1 = "", texto2 = "") {
  return `${texto1} ${texto2}`.trim();
}

// Exercício-04
function concatenarSemNull(texto1 = "", texto2 = "") {
  if (texto1 === null) texto1 = "";
  if (texto2 === null) texto2 = "";
  return `${texto1} ${texto2}`.trim();
}

// Exercício-05
function concatenarEmPaz(texto1 = "", texto2 = "") {
  if (texto1 === null) texto1 = "";
  if (texto2 === null) texto2 = "";
  return `${texto1} ${texto2}`.trim();
}

// Exercício-06
function adicionar(a) {
  return b => a + b;
}

// Exercício-07
function fiboSum(num) {
  if (num === 0) return 0;
  if (num === 1) return 1;
  return fiboSum(num - 1) + fiboSum(num - 2);
}
