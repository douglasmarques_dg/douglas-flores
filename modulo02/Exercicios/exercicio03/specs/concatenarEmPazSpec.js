describe("concatenarEmPaz", function() {
  beforeEach(function() {
    chai.should();
  });

  it("deve concatenar undefined com 'Soja'", function() {
    const texto1 = undefined;
    const texto2 = "Soja";
    const resultado = concatenarEmPaz(texto1, texto2);
    resultado.should.equal("Soja");
  });

  it("deve concatenar 'Soja é bom, mas...' com null", function() {
    const texto1 = "Soja é bom, mas...";
    const texto2 = null;
    const resultado = concatenarEmPaz(texto1, texto2);
    resultado.should.equal("Soja é bom, mas...");
  });

  it("deve concatenar apenas 'Soja é'", function() {
    const texto1 = "Soja é";
    const resultado = concatenarEmPaz(texto1);
    resultado.should.equal("Soja é");
  });
});
