import java.util.*;

public class AtaqueIntercalado implements Estrategia {
    private List<Elfo> formacao = new ArrayList<>(), atacantes = new ArrayList<>();
    
    public List<Elfo> getOrdemDeAtaque(List<Elfo> atacantes) {
        this.atacantes = atacantes;
        int i = 0;
        do {
            if(i % 2 == 0) {
                ElfoVerde eV = getElfoVerde();
                if(eV == null)
                    break;
                else
                    this.formacao.add(eV);                
            } else {
                ElfoNoturno eN = getElfoNoturno();
                if(eN == null)
                    break;
                else
                    this.formacao.add(eN);
            }
            i++;
        }while(this.atacantes.size()>0);
        return this.getFormacao();
    }
    
    protected List<Elfo> getFormacao() {
        return this.formacao;
    }
    
    private ElfoVerde getElfoVerde() {
        if(atacantes.size()-1 == 0) return null;
        ElfoVerde elfo = null;        
        for(int i = 0; i < this.atacantes.size()-1; i++)
            if(this.atacantes.get(i) instanceof ElfoVerde) {
                elfo = (ElfoVerde)(atacantes.get(i));
                this.atacantes.remove(i);
                return elfo;
            }
        return elfo;
    }
    
    private ElfoNoturno getElfoNoturno() {
        if(atacantes.size()-1 == 0) return null;
        ElfoNoturno elfo = null;        
        for(int i = 0; i < this.atacantes.size()-1; i++)
            if(this.atacantes.get(i) instanceof ElfoNoturno) {
                elfo = (ElfoNoturno)(atacantes.get(i));
                this.atacantes.remove(i);
                return elfo;
            }
        return elfo;
    }
}