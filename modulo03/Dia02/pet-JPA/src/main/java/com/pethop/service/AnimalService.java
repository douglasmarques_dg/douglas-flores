package com.pethop.service;

import com.petshop.DAO.AnimalDAO;
import com.petshop.entity.Animal;

public class AnimalService extends GenericService< Animal, Long, AnimalDAO > {
    
    private static AnimalService instance;
    
    static{
        instance = new AnimalService();
    }
    
    public static AnimalService getInstance(){
        return instance;
    }
        
    @Override
    protected AnimalDAO getDao() {
        return AnimalDAO.getInstance();
    }
}