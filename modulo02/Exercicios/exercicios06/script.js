const myClock = document.querySelector("#clock");
const stop = document.querySelector("#stop");
const cont = document.querySelector("#continue");

function code() {
  let clock;
  start();

  function start() {
    clock = new Clock();
    myClock.value = clock.time;
  }

  function keep() {
    clock.keep();
    stop.disabled = false;
  }

  function stopIt() {
    clock.stopIt();
    stop.disabled = true;
  }

  setInterval(start, 1000);

  cont.addEventListener("click", keep);
  stop.addEventListener("click", stopIt);
}

window.addEventListener("load", code());
