import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class NoturnosUltimoTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void setUp() {
        ArrayList<Elfo> elfo = new ArrayList<>();

        Elfo elfo1 = new ElfoVerde("Verde1");
        Elfo elfo2 = new ElfoVerde("Verde2");
        Elfo elfo3 = new ElfoVerde("Verde3");
        Elfo elfo4 = new ElfoNoturno("Noturno1");
        Elfo elfo5 = new ElfoNoturno("Noturno2");

        elfo.add(elfo4);
        elfo.add(elfo1);
        elfo.add(elfo5);
        elfo.add(elfo2);
        elfo.add(elfo3);

        ArrayList<Elfo> esperado = new ArrayList<>();
        Elfo elfo6 = new ElfoVerde("Verde1");
        Elfo elfo7 = new ElfoVerde("Verde2");
        Elfo elfo8 = new ElfoVerde("Verde3");
        Elfo elfo9 = new ElfoNoturno("Noturno1");
        Elfo elfo10 = new ElfoNoturno("Noturno2");

        esperado.add(elfo6);
        esperado.add(elfo7);
        esperado.add(elfo8);
        esperado.add(elfo9);
        esperado.add(elfo10);
        NoturnosUltimo noturnos = new NoturnosUltimo();

        List<Elfo> retornado = noturnos.getOrdemDeAtaque(elfo);

        
        for(int i = 0; i < retornado.size(); i++)
            assertEquals(esperado.get(i).getClass(), retornado.get(i).getClass());
    }
    
    @Test
    public void testarMorto() {
        Elfo elfo6 = new ElfoVerde("Verde1");
        elfo6.setStatus(Status.MORTO);
        ArrayList<Elfo> elfoMorto = new ArrayList<>();
        elfoMorto.add(elfo6);
        NoturnosUltimo noturnos = new NoturnosUltimo();        
        assertEquals(null, noturnos.getOrdemDeAtaque(elfoMorto));
    }
}