package br.com.dbc.locadora.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas.marques
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cliente extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "NOME")
    private String nome;

    @NotNull
    @Column(name = "TELEFONE")
    private String telefone;
    
    @NotNull
    @Column(name = "RUA")
    private String rua;
    
    @NotNull
    @Column(name = "BAIRRO")
    private String bairro;
    
    @NotNull
    @Column(name = "CIDADE")
    private String cidade;
    
    @NotNull
    @Column(name = "ESTADO")
    private String estado;
}
