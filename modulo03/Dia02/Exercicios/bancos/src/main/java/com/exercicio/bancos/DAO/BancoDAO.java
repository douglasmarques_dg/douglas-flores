package com.exercicio.bancos.DAO;

import com.exercicio.bancos.model.Banco;
import com.exercicio.bancos.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author douglas.marques
 */
public class BancoDAO {
    private static BancoDAO instance;
    protected EntityManager entityManager;

    public static BancoDAO getInstance() {
        if (instance == null) {
            instance = new BancoDAO();
        }
        return instance;
    }

    public BancoDAO() {
        entityManager = PersistenceUtil.getEm();
    }

    public Banco getById(final long id) {
        return entityManager.find(Banco.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Banco> findAll() {
        return entityManager.createQuery("FROM " + Banco.class.getName()).getResultList();
    }

    public void persistir(Banco banco) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(banco);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Banco banco) {
        try {
            entityManager.getTransaction().begin();
            if (banco.getIdbanco() == null) {
                entityManager.persist(banco);
            } else {
                banco = entityManager.merge(banco);
            }
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remover(Banco banco) {
        try {
            entityManager.getTransaction().begin();
            banco = entityManager.find(Banco.class, banco.getIdbanco());
            entityManager.remove(banco);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final long id) {
        try {
            Banco banco = getById(id);
            remover(banco);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
