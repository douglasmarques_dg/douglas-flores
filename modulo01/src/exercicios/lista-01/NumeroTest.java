

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * A classe de teste NumeroTest.
 *
 * @author  Douglas Marques
 * @version 09.04.18
 */
public class NumeroTest
{
    @Test
    // Testing an even number
    public void imparTest() {        
        Numero num = new Numero(42);
        assertEquals(false, num.impar());
    }
    
     @Test
    // Testing an odd number
    public void parTest() {
        Numero newNum = new Numero(79);
        assertEquals(true, newNum.impar());
    }
}