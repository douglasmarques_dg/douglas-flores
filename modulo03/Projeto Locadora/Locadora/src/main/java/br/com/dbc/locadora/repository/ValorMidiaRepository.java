package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas.marques
 */
public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {

    public Optional<ValorMidia> findByidMidiaAndFinalVigenciaIsNull(Midia midia);
    
    public Optional<ValorMidia> findByidMidiaTipoAndFinalVigenciaIsNull(MidiaType tipo);

    public List<ValorMidia> findByidMidiaId(Long idMidia);

    public void deleteByIdMidiaId(Long idMidia);
}
