import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.*;

public class ExercitoElfosTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void criarExercitoTest() {
        ExercitoElfos exercitoElfico = new ExercitoElfos();
        for(int i = 0; i < 100; i++) 
            if(i % 2 == 0) exercitoElfico.alistar(new ElfoVerde("NPC" + i));
            else exercitoElfico.alistar(new ElfoNoturno("NPC" + i));
        assertEquals(100, exercitoElfico.vivos().size());
        //assertEquals(0, exercitoElfico.mortos().size());
        assertNull(exercitoElfico.mortos());
    }
    
    @Test
    public void elfoDaLuzIntrusoTest() {
        ExercitoElfos exercitoElfico = new ExercitoElfos();
        exercitoElfico.alistar(new ElfoDaLuz("NPC"));
        assertEquals(0, exercitoElfico.tamanho());
        //assertEquals(0, exercitoElfico.vivos().size());
        assertNull(exercitoElfico.vivos());
        //assertEquals(0, exercitoElfico.mortos().size());
        assertNull(exercitoElfico.mortos());
    }
    
    @Test
    public void buscarElfosVivosExistindoTest() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(vivo);
        ArrayList<Elfo> esperado = new ArrayList<>(Arrays.asList(vivo));
        assertEquals(esperado, exercito.buscar(Status.VIVO));
    }
    
    // NÃO IMPLEMENTADO
    @Test
    public void buscarElfosVivosNaoExistindoTest() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(vivo);
        ArrayList<Elfo> esperado = new ArrayList<>(Arrays.asList(vivo));
        assertEquals(esperado, exercito.buscar(Status.VIVO));
    }
    
    // NÃO IMPLEMENTADO
    @Test
    public void buscarElfosMortosExistindoTest() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(vivo);
        ArrayList<Elfo> esperado = new ArrayList<>(Arrays.asList(vivo));
        assertEquals(esperado, exercito.buscar(Status.VIVO));
    }
    
    @Test
    public void buscarElfosMortosNaoExistindoTest() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(vivo);
        assertNull(exercito.buscar(Status.MORTO));
    }
}