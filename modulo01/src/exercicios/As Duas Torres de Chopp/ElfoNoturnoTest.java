import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void criarElfoNoturnoTest() {
        ElfoNoturno elf = new ElfoNoturno("npc");
        assertEquals("npc", elf.getNome());
        assertEquals(new Item("Arco", 1), elf.inv.obter(0));
        assertEquals(new Item("Flecha", 7), elf.inv.obter(1));
        assertEquals(2, elf.inv.tamanho());
        assertEquals(Status.VIVO, elf.getStatus());
        assertEquals(100, elf.getVida(), 0.1);
    }
    
    @Test
    public void qtdFlechasTest() {
        ElfoNoturno elf = new ElfoNoturno("npc");
        int flechas = elf.getFlecha().getQuantidade();
        Dwarf dwarf = new Dwarf("Oin");
        elf.atirarFlecha(dwarf);
        assertEquals(--flechas, elf.getFlecha().getQuantidade());
    }
    
    @Test
    public void experienciaTest() {
        ElfoNoturno legolas = new ElfoNoturno("Legolas");
        int exp = legolas.getExperiencia();
        Dwarf dwarf = new Dwarf("Gloin");
        legolas.atirarFlecha(dwarf);
        assertEquals(exp+=3, legolas.getExperiencia());
        legolas.atirarFlecha(dwarf);
        assertEquals(exp+=3, legolas.getExperiencia());
    }
    
    @Test
    public void limiteFlechasTest() {
        ElfoNoturno elf = new ElfoNoturno("Fingolfin");
        Dwarf dwarf = new Dwarf("Fili");
        for(int i = 0; i <= 8; i++) elf.atirarFlecha(dwarf);
        assertEquals(0, elf.getFlecha().getQuantidade());
        assertEquals(21, elf.getExperiencia());
        assertEquals(Status.MORTO, elf.getStatus());
    }
    
    @Test
    public void atirarFlechaTest(){
        ElfoNoturno elf = new ElfoNoturno("Ciramyr");
        Dwarf dwarf = new Dwarf("Kili");
        elf.atirarFlecha(dwarf);
        assertEquals(6, elf.getFlecha().getQuantidade());
        assertEquals(3, elf.getExperiencia());
        assertEquals(100, dwarf.getVida(), 0.1);
    }
    
    @Test
    public void ganharItemTest() {
        ElfoNoturno elf = new ElfoNoturno("npc");        
        elf.ganharItem(new Item("Espada de aço valiriano", 1));
        assertEquals(new Item("Espada de aço valiriano", 1), elf.inv.obter(2));
        assertEquals(3, elf.inv.tamanho());
        elf.ganharItem(new Item("Espada de aço valiriano", 2));
        assertEquals(3, elf.inv.tamanho());
        assertEquals(new Item("Espada de aço valiriano", 3), elf.inv.obter(2));
        elf.ganharItem(new Item("Escudo de madeira", 1));
        assertEquals(4, elf.inv.tamanho());
        assertEquals(new Item("Escudo de madeira", 1), elf.inv.buscarItem("Escudo de madeira"));
    }
}