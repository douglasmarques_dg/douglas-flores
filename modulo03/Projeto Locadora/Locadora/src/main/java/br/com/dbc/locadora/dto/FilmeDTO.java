package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Categoria;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author douglas.marques
 */
@Data
@Builder
public class FilmeDTO implements Serializable {

    private Long id;

    private String titulo;

    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate lancamento;

    private Categoria categoria;

    private ArrayList<MidiaDTO> midia;
}
