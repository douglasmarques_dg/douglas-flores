public class DwarfBarbaLonga extends Dwarf{
    Dado D6;
    
    public DwarfBarbaLonga(String nome, Dado dado) {
        super(nome);
        this.D6 = dado;
    }
    
    public boolean receberDano() {        
        if(this.D6.sortear() <= 2) return false;
        return super.receberDano();
    }
}