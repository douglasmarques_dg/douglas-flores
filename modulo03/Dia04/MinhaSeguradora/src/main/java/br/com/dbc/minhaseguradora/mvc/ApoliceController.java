package br.com.dbc.minhaseguradora.mvc;

import br.com.dbc.minhaseguradora.entity.Apolice;
import br.com.dbc.minhaseguradora.service.ApoliceService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author douglas.marques
 */
@Controller
public class ApoliceController {

    @Autowired
    private ApoliceService aS;

    @GetMapping("/")
    public String findAll(Model model) {
        List<Apolice> apolices = aS.findAll(PageRequest.of(0, 10)).getContent();
        model.addAttribute("apolices", apolices);
        return "Apolice";
    }

    @PostMapping("/")
    public String save(Apolice apolice, Model model) {
        aS.save(apolice);
        List<Apolice> apolices = aS.findAll(PageRequest.of(0, 10)).getContent();
        model.addAttribute("apolices", apolices);
        return "Apolice";
    }
}
