public class DadoD6FAKE implements Dado{
    private int valorFalso;
    
    public void setValorFalso(int valorFalso) {
        if(valorFalso > 0 && valorFalso < 7) this.valorFalso = valorFalso;
    }
    
    public int sortear() {
        return valorFalso;
    }
}