package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.UserDTO;
import br.com.dbc.locadora.entity.AbstractEntity;
import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.repository.UserRepository;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author douglas.marques
 */
public class UserRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private UserRestController userRestController;

    @Autowired
    private UserRepository userRepository;

    @Override
    protected AbstractRestController getController() {
        return userRestController;
    }

    @Before
    public void beforeTest() {
        userRepository.deleteAll();
    }

    @After
    public void tearDown() {

    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void createUserTest() throws Exception {
        AbstractEntity En = User.builder()
                .firstName("João")
                .lastName("Doe")
                .username("joaodoe")
                .password("123joao")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(En)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        List<User> users = userRepository.findAll();
        User user = (User) En;
        Assert.assertEquals(1, users.size());
        Assert.assertEquals(users.get(0).getFirstName(), user.getFirstName());
        Assert.assertEquals(users.get(0).getLastName(), user.getLastName());
        Assert.assertEquals(users.get(0).getUsername(), user.getUsername());
        Assert.assertEquals(users.get(0).getPassword(), user.getPassword());
    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void changePasswordTest() throws Exception {
        AbstractEntity En = User.builder()
                .firstName("João")
                .lastName("Doe")
                .username("joaodoe")
                .password("123joao")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(En)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        User user = (User) En;

        UserDTO userDTO = UserDTO.builder()
                .username("joaodoe")
                .password("teste")
                .build();
        User u = objectMapper.readValue(restMockMvc.perform(MockMvcRequestBuilders.put("/api/user/password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(userDTO)))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn().getResponse().getContentAsString(), User.class);

        Assert.assertEquals(user.getFirstName(), u.getFirstName());
        Assert.assertEquals(user.getLastName(), u.getLastName());
        Assert.assertEquals(user.getUsername(), u.getUsername());
        Assert.assertEquals(userDTO.getPassword(), u.getPassword());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testGetUsers() throws Exception {
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/user"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    
    @Test(expected = Exception.class)
    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    public void changePasswordDeniedTest() throws Exception {
        AbstractEntity En = User.builder()
                .firstName("João")
                .lastName("Doe")
                .username("joaodoe")
                .password("123joao")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(En)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        User user = (User) En;

        UserDTO userDTO = UserDTO.builder()
                .username("joaodoe")
                .password("teste")
                .build();
        User u = objectMapper.readValue(restMockMvc.perform(MockMvcRequestBuilders.put("/api/user/password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(userDTO)))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn().getResponse().getContentAsString(), User.class);

        Assert.assertEquals(user.getFirstName(), u.getFirstName());
        Assert.assertEquals(user.getLastName(), u.getLastName());
        Assert.assertEquals(user.getUsername(), u.getUsername());
        Assert.assertEquals(userDTO.getPassword(), u.getPassword());
    }
}
