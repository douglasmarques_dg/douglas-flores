package com.petshop.DAO;

import com.petshop.entity.Cliente;

/**
 *
 * @author douglas.marques
 */
public class ClienteDAO extends GenericDAO<Cliente, Long>{

    private static final ClienteDAO INSTANCE;
    
    static {
        INSTANCE = new ClienteDAO();
    }

    public static ClienteDAO getInstance() {
        return INSTANCE;
    }
    
    @Override
    protected Class<Cliente> getEntityClass() {
        return Cliente.class;
    }

    @Override
    protected String getIdProperty() {
        return "id";
    }
}
