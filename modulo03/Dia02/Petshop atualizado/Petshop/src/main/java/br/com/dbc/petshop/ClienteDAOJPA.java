package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.PersistenceUtils;
import java.util.List;
import javax.persistence.EntityManager;

public class ClienteDAOJPA {

    private static ClienteDAOJPA instance;
    protected EntityManager entityManager;

    public static ClienteDAOJPA getInstance() {
        if (instance == null) instance = new ClienteDAOJPA();
        return instance;
    }

    public ClienteDAOJPA() {
        entityManager = PersistenceUtils.getEm();
    }

    public Cliente getById(final long id) {
        return entityManager.find(Cliente.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Cliente> findAll() {
        return entityManager.createQuery("FROM " + Cliente.class.getName()).getResultList();
    }

    public void persistir(Cliente cliente) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(cliente);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Cliente cliente) {
        try {
            entityManager.getTransaction().begin();
            if (cliente.getId() == null) {
                entityManager.persist(cliente);
            } else {
                cliente = entityManager.merge(cliente);
            }
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remover(Cliente cliente) {
        try {
            entityManager.getTransaction().begin();
            cliente = entityManager.find(Cliente.class, cliente.getId());
            entityManager.remove(cliente);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id) {
        try {
            Cliente cliente = getById(id);
            remover(cliente);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
