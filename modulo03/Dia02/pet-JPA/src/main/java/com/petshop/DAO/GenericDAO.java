package com.petshop.DAO;

import static java.lang.String.format;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;

/**
 *
 * @author douglas.marques
 */
public abstract class GenericDAO<ENTITY, ID> {

    private static final Logger LOG = Logger.getLogger(GenericDAO.class.getName());
    private static final EntityManager ENTITYMANAGER = PersistenceUtil.getEm();

    protected abstract Class<ENTITY> getEntityClass();

    protected abstract String getIdProperty();

    /*public List<ENTITY> findAll() {
        EntityManager em = PersistenceUtil.getEm();
        return em.createQuery("select c from Entity c").getResultList();
    }*/
    public List<ENTITY> findAll() {
        return ENTITYMANAGER.createQuery("FROM " + getEntityClass().getName()).getResultList();
    }

    /*public Entity findOne() {
        ENTITYMANAGER em = PersistenceUtil.getEm();
        return (Entity)em.createQuery("select c from Entity c where idEntity is c.idEntity").getSingleResult();
    }*/
    public ENTITY findOne(ID id) {
        return ENTITYMANAGER.find(getEntityClass(), id);
    }

    /*public ENTITY findOne(ID id) {
       EntityManager em = PersistenceUtil.getEm();
       return em.createQuery(
               format("select e from %s e where e.id = :id", getEntityClass().getSimpleName()),
               getEntityClass())
               .setParameter("id", id)
               .getSingleResult();
   }*/
    public void create(ENTITY entity) {
        try {
            ENTITYMANAGER.getTransaction().begin();
            ENTITYMANAGER.persist(entity);
            ENTITYMANAGER.getTransaction().commit();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            ENTITYMANAGER.getTransaction().rollback();
        }
    }

    public void update(ENTITY entity) {
        try {
            ENTITYMANAGER.getTransaction().begin();
            ENTITYMANAGER.merge(entity);
            ENTITYMANAGER.getTransaction().commit();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            ENTITYMANAGER.getTransaction().rollback();
        }
    }

    public void delete(ENTITY entity) {
        try {
            ENTITYMANAGER.getTransaction().begin();
            //entity = ENTITYMANAGER.find(Entity.class, entity.getId());
            ENTITYMANAGER.remove(entity);
            ENTITYMANAGER.getTransaction().commit();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            ENTITYMANAGER.getTransaction().rollback();
        }
    }
}
