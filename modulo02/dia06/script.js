function imprimeData() {
  const titulo = document.querySelector("#titulo");
  const data = new Date();
  titulo.innerHTML = data;
}

imprimeData();

function relogio() {
  setInterval(function() {
    titulo.innerHTML = new Date();
  }, 1000);
}

relogio();

// function otoRelogio() {
//   setInterval(function() {
//     console.log(new Date());
//   }, 1500);
// }

// otoRelogio();

function pararRelogio() {
  const parar = document.querySelector("#pararRelogio");
  parar.addEventListener("click", clearInterval());
}
