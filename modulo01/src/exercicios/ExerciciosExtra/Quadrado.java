public class Quadrado extends Retangulo {
    public Quadrado(int x) {
        super(0,0);
        this.setX(x);
    }
    
    public void setX(int x) {
        this.x = x;
        this.y = x;
    }
    
    public void setY(int y) {
        this.y = y;
        this.x = y;
    }
}