import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import java.util.ArrayList;

public class PaginadorInventarioTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    public PaginadorInventario automate() {
        Inventario itens = new Inventario(10);
        Item[] itensArr = {
            new Item("Machado", 1),
            new Item("Lança", 3),
            new Item("Arco", 2),
            new Item("Flecha", 9),
            new Item("Poção de cura", 6),
            new Item("Poção de Magia", 2)
        };
        for(Item i : itensArr) itens.adicionarItem(i);
        return new PaginadorInventario(itens);
    }
    
    @Test
    public void pularTest() {        
        PaginadorInventario pInv = automate();
        assertEquals(0, pInv.getIndex());
        pInv.pular(1);
        assertEquals(1, pInv.getIndex());
        pInv.pular(2);
        assertEquals(3, pInv.getIndex());
    }
    
    @Test
    public void pularFailTest() {        
        PaginadorInventario pInv = automate();
        assertEquals(0, pInv.getIndex());
        pInv.pular(-1);
        assertEquals(0, pInv.getIndex());
        pInv.pular(99);
        assertEquals(0, pInv.getIndex());
    }
    
    @Test
    public void limitarTest() {
        Inventario inventario = new Inventario(10);
        inventario.adicionarItem(new Item("Espada", 1));
        inventario.adicionarItem(new Item("Escudo de metal", 2));
        inventario.adicionarItem(new Item("Poção de HP", 3));
        inventario.adicionarItem(new Item("Bracelete", 1));
        
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        paginador.pular(0);         
        List<Item> itens = new ArrayList<>();
        itens.add(new Item("Espada", 1));
        itens.add(new Item("Escudo de metal", 2));
        for(int i=0; i < 2; i++) assertEquals(itens.get(i).getDescricao(), paginador.limitar(2).get(i).getDescricao());
        // retorna os itens "Espada” e "Escudo de metal”
        
        paginador.pular(2);
        List<Item> itens2 = new ArrayList<>();
        itens2.add(new Item("Poção de HP", 3));
        itens2.add(new Item("Bracelete", 1));
        for(int j=0; j < 2; j++) assertEquals(itens2.get(j).getDescricao(), paginador.limitar(2).get(j).getDescricao());
        // retorna os itens "Poção de HP” e "Bracelete”    
    }
}