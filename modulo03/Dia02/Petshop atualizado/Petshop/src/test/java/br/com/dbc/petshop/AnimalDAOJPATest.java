package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author douglas.marques
 */
public class AnimalDAOJPATest {

    public AnimalDAOJPATest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        PersistenceUtils.beginTransaction();
        PersistenceUtils.getEm().createNativeQuery("delete from animal").executeUpdate();
        PersistenceUtils.getEm().createNativeQuery("delete from cliente").executeUpdate();
        PersistenceUtils.getEm().getTransaction().commit();
    }

    /**
     * Test of persistir method, of class AnimalDAOJPA. JPA
     */
    @Test
    public void testPersistir() {
        System.out.println("persistir");

        AnimalDAOJPA instanceAnimal = new AnimalDAOJPA();
        ClienteDAOJPA instanceCliente = new ClienteDAOJPA();

        Cliente c = new Cliente(null, "Cliente teste", SexoType.F, "Profissao teste");
        instanceCliente.persistir(c);

        long preco = 999;
        for (int i = 0; i <= 9; i++) {
            Animal animal = new Animal(null, String.format("Animal {i}", i), SexoType.F, c, preco);
            instanceAnimal.persistir(animal);
        }

        List<Animal> resultado = instanceAnimal.findAll();
        assertEquals(10, resultado.size());
    }

    /**
     * Test of remover method, of class ClienteDAOJPA. JPA
     */
    @Test
    public void testRemover() {
        System.out.println("remover");

        ClienteDAOJPA instanceCliente = new ClienteDAOJPA();
        AnimalDAOJPA instanceAnimal = new AnimalDAOJPA();

        Cliente cliente = new Cliente(null, "Cliente para teste", SexoType.M, "Profissao teste");
        instanceCliente.persistir(cliente);

        long preco = 999;
        Animal animal = new Animal(null, "Animal para remover", SexoType.F, cliente, preco);
        instanceAnimal.persistir(animal);

        List<Animal> resultado = instanceAnimal.findAll();
        assertEquals(1, resultado.size());

        instanceAnimal.remover(animal);

        resultado = instanceAnimal.findAll();
        assertEquals(0, resultado.size());
    }

    /**
     * Test of merge method, of class ClienteDAOJPA. JPA
     */
    @Test
    public void testMerge() {
        System.out.println("merge");

        AnimalDAOJPA instanceAnimal = new AnimalDAOJPA();
        ClienteDAOJPA instanceCliente = new ClienteDAOJPA();

        Cliente c = new Cliente(null, "Cliente teste", SexoType.F, "Profissao teste");
        instanceCliente.merge(c);

        long preco = 999;
        Animal animal = new Animal(null, "Animal teste", SexoType.F, c, preco);
        instanceAnimal.merge(animal);

        List<Animal> resultado = instanceAnimal.findAll();
        assertEquals(1, resultado.size());

        animal.setNome("Animal atualizado");
        instanceAnimal.merge(animal);

        resultado = instanceAnimal.findAll();
        assertEquals(1, resultado.size());
    }
}
