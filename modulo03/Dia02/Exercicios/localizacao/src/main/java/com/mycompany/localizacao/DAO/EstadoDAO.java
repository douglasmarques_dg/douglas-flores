package com.mycompany.localizacao.DAO;

import com.mycompany.localizacao.model.Estado;
import com.mycompany.localizacao.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;

public class EstadoDAO {

    private static EstadoDAO instance;
    protected EntityManager entityManager;

    public static EstadoDAO getInstance() {
        if (instance == null) {
            instance = new EstadoDAO();
        }
        return instance;
    }

    public EstadoDAO() {
        entityManager = PersistenceUtil.getEm();
    }

    public Estado getById(final long id) {
        return entityManager.find(Estado.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Estado> findAll() {
        return entityManager.createQuery("FROM " + Estado.class.getName()).getResultList();
    }

    public void persistir(Estado estado) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(estado);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Estado estado) {
        try {
            entityManager.getTransaction().begin();
            if (estado.getIdestado()== null) {
                entityManager.persist(estado);
            } else {
                estado = entityManager.merge(estado);
            }
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remover(Estado estado) {
        try {
            entityManager.getTransaction().begin();
            estado = entityManager.find(Estado.class, estado.getIdestado());
            entityManager.remove(estado);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final long id) {
        try {
            Estado estado = getById(id);
            remover(estado);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
