package com.minha.floricultura.REST;

import com.minha.floricultura.DAO.VendaDAO;
import com.minha.floricultura.entity.Venda;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

/**
 *
 * @author douglas.marques
 */
@Stateless
@Path("venda")
public class VendaFacadeREST extends AbstractFacade<Venda, VendaDAO> {

    @Inject
    private VendaDAO vendaDAO;
    
    @Override
    protected VendaDAO getDAO() {
        return vendaDAO;
    }
}
