describe("concatenarSemNull", function() {
  beforeEach(function() {
    chai.should();
  });

  it("deve concatenar null com 'Soja'", function() {
    const texto1 = null;
    const texto2 = "Soja";
    const resultado = concatenarSemNull(texto1, texto2);
    resultado.should.equal("Soja");
  });

  it("deve concatenar 'Soja' com 'é bom'", function() {
    const texto1 = "Soja";
    const texto2 = "é bom";
    const resultado = concatenarSemNull(texto1, texto2);
    resultado.should.equal("Soja é bom");
  });

  it("deve concatenar apenas 'Soja é'", function() {
    const texto1 = "Soja é";
    const resultado = concatenarSemNull(texto1);
    resultado.should.equal("Soja é");
  });
});
