describe("adicionar", function() {
  beforeEach(function() {
    chai.should();
  });

  it("deve calcular com método currying 3 + 4", function() {
    const num1 = 3;
    const num2 = 4;
    const resultado = adicionar(num1)(num2);
    resultado.should.equal(7);
  });

  it("deve calcular com método currying 5642 + 8749", function() {
    const num1 = 5642;
    const num2 = 8749;
    const resultado = adicionar(num1)(num2);
    resultado.should.equal(14391);
  });
});
