package com.minha.floricultura.WS;

import com.minha.floricultura.DAO.AbstractDAO;
import java.util.List;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.validation.Valid;

/**
 *
 * @author douglas.marques
 */
public abstract class AbstractCRUDWS<DAO extends AbstractDAO<E>, E> {

    public abstract DAO getDAO();

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@Valid @WebParam(name = "entity") E entity) {
        this.getDAO().create(entity);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@Valid @WebParam(name = "entity") E entity) {
        this.getDAO().edit(entity);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "entity") E entity) {
        this.getDAO().remove(entity);
    }

    @WebMethod(operationName = "find")
    public E find(@WebParam(name = "id") Long id) {
        return this.getDAO().find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<E> findAll() {
        return this.getDAO().findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<E> findRange(@WebParam(name = "range") int[] range) {
        return this.getDAO().findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return this.getDAO().count();
    }
}
