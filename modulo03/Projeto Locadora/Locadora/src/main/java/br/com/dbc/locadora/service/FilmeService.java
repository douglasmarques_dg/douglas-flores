package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.PrecoDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.FilmeRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas.marques
 */
@Service
public class FilmeService extends AbstractCRUDService<Filme, Long> {

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Filme, Long> getRepository() {
        return filmeRepository;
    }

    private Filme dtoToFilme(FilmeDTO dto) {
        Filme filme = filmeRepository.save(Filme.builder()
                .id(dto.getId() == null ? null : dto.getId())
                .titulo(dto.getTitulo())
                .lancamento(dto.getLancamento())
                .categoria(dto.getCategoria())
                .build());
        return filme;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Optional<Filme> save(FilmeDTO dto) {
        Filme filme = filmeRepository.save(Filme.builder()
                .titulo(dto.getTitulo())
                .lancamento(dto.getLancamento())
                .categoria(dto.getCategoria())
                .build());
        dto.getMidia().forEach((midia) -> {
            midiaService.saveMidias(filme, midia);
        });
        return findById(filme.getId());
    }

    public Page<Filme> findByCategoria(Pageable pageable, Categoria categoria) {
        return filmeRepository.findByCategoria(pageable, categoria);
    }

    public Page<Filme> findByTitulo(Pageable pageable, String titulo) {
        return filmeRepository.findByTituloContainingIgnoreCase(pageable, titulo);
    }

    public Page<Filme> findByLancamento(Pageable pageable, LocalDate lancamento) {
        return filmeRepository.findByLancamento(pageable, lancamento);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme updateFilmeAndMidia(FilmeDTO dto) {
        Filme filme = dtoToFilme(dto);
        dto.getMidia().forEach(midiaDTO -> midiaService.updateMidiaDTO(midiaDTO, filme));
        return filme;
    }

    public int countByTipo(Long id, MidiaType tipo) {
        return midiaService.countByTipoFilme(id, tipo);
    }

    private PrecoDTO valorMidiaToValorMidiaDTO(ValorMidia valor) {
        PrecoDTO pDTO = PrecoDTO.builder()
                .id(valor.getId())
                .tipo(valor.getIdMidia().getTipo())
                .valor(valor.getValor())
                .inicioVigencia(valor.getInicioVigencia())
                .finalVigencia(valor.getFinalVigencia())
                .build();
        return pDTO;
    }

    public List<PrecoDTO> findPrecos(Long idFilme) {
        List<PrecoDTO> precos = new ArrayList<>();
        List<Midia> midias = midiaService.findByIdFilme(idFilme);
        midias.forEach((midia) -> {
            List<ValorMidia> valores = valorMidiaService.findByidMidiaId(midia.getId());
            valores.forEach((valor) -> {
                precos.add(valorMidiaToValorMidiaDTO(valor));
            });
        });
        return precos;
    }

    public Page<CatalogoDTO> catalogoDeFilmes(Pageable pageable, FilmeDTO nome) {
        List<CatalogoDTO> catalogo = new ArrayList<>();
        Page<Filme> filmes = filmeRepository.findByTituloContainingIgnoreCase(pageable, nome.getTitulo());
        filmes.forEach((filme) -> {
            List<Midia> midias = midiaService.findByIdFilmeSemAluguel(filme.getId());
            HashMap<MidiaType, Double> valores = new HashMap<>();
            midias.forEach((midia) -> {
                if (midia.getTipo() == MidiaType.DVD) {
                    valores.put(MidiaType.DVD, valorMidiaService.findByMidia(midia));
                } else if (midia.getTipo() == MidiaType.VHS) {
                    valores.put(MidiaType.VHS, valorMidiaService.findByMidia(midia));
                } else if (midia.getTipo() == MidiaType.BLUE_RAY) {
                    valores.put(MidiaType.BLUE_RAY, valorMidiaService.findByMidia(midia));
                }
            });
            catalogo.add(buildaCatalogo(filme, midias.size() > 0, valores));
        });      
        return new PageImpl<>(catalogo, pageable, filmes.getTotalElements());
    }

    private CatalogoDTO buildaCatalogo(Filme filme, boolean disponivel, HashMap<MidiaType, Double> valores) {
        return CatalogoDTO.builder()
                .filme(filme)
                .disponibilidade(disponivel ? "Disponivel!" : "Não disponível!")
                .valores(valores)
                .build();
    }
}
