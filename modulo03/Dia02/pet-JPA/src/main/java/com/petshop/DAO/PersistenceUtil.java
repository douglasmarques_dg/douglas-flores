package com.petshop.DAO;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author douglas.marques
 */
public class PersistenceUtil {

    static EntityManager em;

    static {
        em = Persistence.createEntityManagerFactory("pet-jpa").createEntityManager();
    }

    public static EntityManager getEm() {
        return em;
    }

    public static void beginTransaction() {
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
    }
}