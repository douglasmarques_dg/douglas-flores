package com.minha.floricultura.REST;

import com.minha.floricultura.DAO.ProdutoDAO;
import com.minha.floricultura.entity.Produto;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author douglas.marques
 */
@Stateless
@Path("produto")
public class ProdutoFacadeREST extends AbstractFacade<Produto, ProdutoDAO> {

    @Inject
    private ProdutoDAO produtoDAO;

    @Override
    protected ProdutoDAO getDAO() {
        return produtoDAO;
    }

    @GET
    @Path("find/{descricao}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findAll(@PathParam("descricao") String descricao) {
        return Response.ok(produtoDAO.findDescricao(descricao)).build();
    }
}
