import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void naoHaElfosTest() {
        assertEquals(0, Elfo.quantElfos());
    }
    
    @Test
    public void contarPelotaoElficoTest() {
        ExercitoElfos exercitoElfico = new ExercitoElfos();
        for(int i = 0; i < 100; i++) 
            if(i % 2 == 0) exercitoElfico.alistar(new ElfoVerde("NPC" + i));
            else exercitoElfico.alistar(new ElfoNoturno("NPC" + i));
        assertEquals(100, Elfo.quantElfos());
    }
    
    @Test
    public void criarElfoTest() {
        Elfo elf = new Elfo("Glorfindel");
        assertEquals("Glorfindel", elf.getNome());
        assertEquals(new Item("Arco", 1), elf.inv.obter(0));
        assertEquals(new Item("Flecha", 7), elf.inv.obter(1));
        assertEquals(Status.VIVO, elf.getStatus());
        assertEquals(100, elf.getVida(), 0.1);
    }
    
    @Test
    public void qtdFlechasTest() {
        Elfo elf = new Elfo("Gil-Galad");
        int flechas = elf.getFlecha().getQuantidade();
        Dwarf dwarf = new Dwarf("Oin");
        elf.atirarFlecha(dwarf);
        assertEquals(--flechas, elf.getFlecha().getQuantidade());
    }
    
    @Test
    public void experienciaTest() {
        Elfo legolas = new Elfo("Legolas");
        int exp = legolas.getExperiencia();
        Dwarf dwarf = new Dwarf("Gloin");
        legolas.atirarFlecha(dwarf);
        assertEquals(++exp, legolas.getExperiencia());
    }
    
    @Test
    public void limiteFlechasTest() {
        Elfo elf = new Elfo("Fingolfin");
        Dwarf dwarf = new Dwarf("Fili");
        for(int i = 0; i <= 8; i++) elf.atirarFlecha(dwarf);
        assertEquals(0, elf.getFlecha().getQuantidade());
        assertEquals(7, elf.getExperiencia());
    }
    
    @Test
    public void atirarFlechaTest(){
        Elfo elf = new Elfo("Ciramyr");
        Dwarf dwarf = new Dwarf("Kili");
        elf.atirarFlecha(dwarf);
        assertEquals(6, elf.getFlecha().getQuantidade());
        assertEquals(1, elf.getExperiencia());
        assertEquals(100, dwarf.getVida(), 0.1);
    }
    
    @Test
    public void ganharItemTest() {
        Elfo elf = new Elfo("npc");        
        elf.ganharItem(new Item("Espada de aço valiriano", 1));
        assertEquals(new Item("Espada de aço valiriano", 1), elf.inv.obter(2));
        assertEquals(3, elf.inv.tamanho());        
        assertEquals(new Item("Espada de aço valiriano", 1), elf.inv.obter(2));
        elf.ganharItem(new Item("Espada de aço valiriano", 2));
        assertEquals(new Item("Espada de aço valiriano", 3), elf.inv.obter(2));
        elf.ganharItem(new Item("Escudo de madeira", 1));
        assertEquals(new Item("Escudo de madeira", 1), elf.inv.buscarItem("Escudo de madeira"));
    }
}