public class Retangulo {
    protected int x, y;
    
    public Retangulo(int x, int y) {
         this.x = x;
         this.y = y;
    }
    
    public void setX(int x) {
        this.x = x;
    }
    
    public void setY(int y) {
        this.y = y;
    }
    
    public int getY() {
        return this.y;
    }
    
    public int getX() {
        return this.x;
    }
    
    public int calcularArea() {
        return this.x * this.y;
    }
}