class Clock {
  constructor() {
    this._time = this.setClock();
    this._up = setInterval(() => {
      this.time = this.setClock();
    }, 1000);
  }

  get time() {
    return this._time;
  }

  set time(time) {
    this._time = time;
  }

  setClock() {
    this._date = new Date();
    this._hour = this._date.getHours();
    this._min = this._date.getMinutes();
    this._sec = this._date.getSeconds();

    if (this._hour < 10) this._hour = `0${this._hour}`;
    if (this._min < 10) this._min = `0${this._min}`;
    if (this._sec < 10) this._sec = `0${this._sec}`;

    return `${this._hour}:${this._min}:${this._sec}`;
  }

  keep() {
    this._up;
  }

  stopIt() {
    clearInterval(this._up);
    console.log("É, não tá parando, eu sei :/");
  }
}
