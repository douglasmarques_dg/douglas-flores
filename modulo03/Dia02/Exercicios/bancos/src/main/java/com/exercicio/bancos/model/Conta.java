package com.exercicio.bancos.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author douglas.marques
 */
@Entity
@Table(name = "CONTA")
@NamedQueries({
    @NamedQuery(name = "Conta.findAll", query = "SELECT c FROM Conta c")
    , @NamedQuery(name = "Conta.findByIdconta", query = "SELECT c FROM Conta c WHERE c.idconta = :idconta")
    , @NamedQuery(name = "Conta.findByTipoconta", query = "SELECT c FROM Conta c WHERE c.tipoconta = :tipoconta")})
public class Conta implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDCONTA")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTA_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    private Long idconta;
    @Basic(optional = false)
    @Column(name = "TIPOCONTA")
    private String tipoconta;
    @JoinColumn(name = "IDBANCO", referencedColumnName = "IDBANCO")
    @ManyToOne(optional = false)
    private Banco idbanco;
    @JoinColumn(name = "IDCLIENTE", referencedColumnName = "IDCLIENTE")
    @ManyToOne(optional = false)
    private Cliente idcliente;

    public Conta() {
    }

    public Conta(Long idconta) {
        this.idconta = idconta;
    }

    public Conta(Long idconta, String tipoconta) {
        this.idconta = idconta;
        this.tipoconta = tipoconta;
    }

    public Long getIdconta() {
        return idconta;
    }

    public void setIdconta(Long idconta) {
        this.idconta = idconta;
    }

    public String getTipoconta() {
        return tipoconta;
    }

    public void setTipoconta(String tipoconta) {
        this.tipoconta = tipoconta;
    }

    public Banco getIdbanco() {
        return idbanco;
    }

    public void setIdbanco(Banco idbanco) {
        this.idbanco = idbanco;
    }

    public Cliente getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Cliente idcliente) {
        this.idcliente = idcliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idconta != null ? idconta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conta)) {
            return false;
        }
        Conta other = (Conta) object;
        if ((this.idconta == null && other.idconta != null) || (this.idconta != null && !this.idconta.equals(other.idconta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exercicio.bancos.model.Conta[ idconta=" + idconta + " ]";
    }
    
}
