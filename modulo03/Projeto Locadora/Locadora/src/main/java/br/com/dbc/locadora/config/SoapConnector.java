package br.com.dbc.locadora.config;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author douglas.marques
 */
public class SoapConnector extends WebServiceGatewaySupport {
 
    public Object callWebService(Object request){
        return getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
