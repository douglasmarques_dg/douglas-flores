package br.com.dbc.locadora.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas.marques
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValorMidia extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "VALOR")
    private double valor;

    @NotNull
    @Column(name = "INICIO_VIGENCIA")
    private LocalDateTime inicioVigencia;

    @Column(name = "FINAL_VIGENCIA")
    private LocalDateTime finalVigencia;

    @JoinColumn(name = "ID_MIDIA", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Midia idMidia;
}
