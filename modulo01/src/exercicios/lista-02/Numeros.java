public class Numeros {
    private double[] entrada;
    
    public Numeros(double[] entrada) {
        this.entrada = entrada;
    }
    
    public double[] getNumeros() {
        return this.entrada;
    }
    
    public double[] calcularMediaSeguinte() {
        if(this.entrada == null) return null;
        double[] entMedia = new double[this.entrada.length-1];
        for(int i = 0; i < this.entrada.length-1; i++) 
            entMedia[i] = (this.entrada[i] + this.entrada[i+1]) / 2;
        return entMedia;
    }
}