package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Role;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author douglas.marques
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
    
}
