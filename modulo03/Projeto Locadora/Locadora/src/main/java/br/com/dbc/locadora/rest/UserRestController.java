package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.UserDTO;
import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author douglas.marques
 */
@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class UserRestController extends AbstractRestController<User, Long, UserService> {
    
    @Autowired
    private UserService userService;
    
    @Override
    protected UserService getService() {
        return userService;
    }

    @PutMapping("/password")
    public ResponseEntity<?> changePassword(@RequestBody UserDTO user) {
        if(user.getUsername()== null || user.getPassword() == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(userService.updatePassword(user));
    }
}
