import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemImperdivelTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void naoPerdeItemTest() {
        ItemImperdivel i = new ItemImperdivel("Imperdivel", 1);
        i.setQuantidade(0);
        assertEquals(1, i.getQuantidade());
    }
    
    @Test
    public void naoPerdeItemExtremeTest() {
        ItemImperdivel i = new ItemImperdivel("Imperdivel", 1);
        i.setQuantidade(-100);
        assertEquals(1, i.getQuantidade());
    }
    
    @Test
    public void criarItemImperdivelComZeroQuantidadesTest() {
        ItemImperdivel i = new ItemImperdivel("Imperdivel", 0);
        assertEquals(1, i.getQuantidade());
    }
}