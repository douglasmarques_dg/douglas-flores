package com.minha.floricultura.REST;

import com.minha.floricultura.DAO.ClienteDAO;
import com.minha.floricultura.entity.Cliente;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

/**
 *
 * @author douglas.marques
 */
@Stateless
@Path("cliente")
public class ClienteFacadeREST extends AbstractFacade<Cliente, ClienteDAO> {

    @Inject
    private ClienteDAO clienteDAO;

    @Override
    protected ClienteDAO getDAO() {
        return clienteDAO;
    }
}
