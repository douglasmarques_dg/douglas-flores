package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.repository.AluguelRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas.marques
 */
@Service
public class AluguelService extends AbstractCRUDService<Aluguel, Long> {

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ClienteService clienteService;

    @Override
    protected JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Optional<Aluguel> save(AluguelDTO dto) {
        Aluguel aluguel = aluguelRepository.save(Aluguel.builder()
                .retirada(LocalDateTime.now())
                .previsao(LocalDateTime.now())
                .devolucao(null)
                .multa(0.0)
                .idCliente(clienteService.findById(dto.getIdCliente()).orElseGet(null))
                .build());
        dto.getMidias().forEach((midia) -> {
            Midia m = midiaService.findById(midia).orElseGet(null);
            if (m.getIdAluguel() == null) {
                midiaService.edit(m, aluguel);
            }
        });
        return findById(aluguel.getId());
    }

    public Page<Aluguel> buscarHoje(Pageable pageable) {
        return aluguelRepository.findByPrevisaoBetween(pageable, LocalDate.now().atStartOfDay(), LocalDate.now().atStartOfDay().plusHours(16));
    }
}
