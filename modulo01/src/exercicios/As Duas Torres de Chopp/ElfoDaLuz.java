public class ElfoDaLuz extends Elfo {
    private int numAtq;
    private final ItemImperdivel Galvorn = new ItemImperdivel("Espada de Galvorn", 1);
    private static final double QTD_VIDA_GANHA = 10; 
    
    public ElfoDaLuz(String nome) {        
        super(nome);
        QTD_DANO = 21;
        this.numAtq = 0;
        inv.adicionarItem(Galvorn);
    } 
    
    public void atacarComEspada(Dwarf dwarf) {
        numAtq++;
        if(getStatus() == Status.MORTO) return;
        if(numAtq % 2 == 0) ganharVida(this.QTD_VIDA_GANHA);
        else {
            receberDano();
            if(getStatus() == Status.MORTO) return;
        }
        if(dwarf.receberDano()) experiencia+=QTD_XP;
    }
    
    public void perdeItem(Item i) {
        if(i instanceof ItemImperdivel) return;
        else super.perdeItem(i);
    }
}