import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void criarElfoDaLuzTest() {
        ElfoDaLuz elf = new ElfoDaLuz("npc");
        assertEquals("npc", elf.getNome());
        assertEquals(new Item("Arco", 1), elf.inv.obter(0));
        assertEquals(new Item("Flecha", 7), elf.inv.obter(1));
        assertEquals(new Item("Espada de Galvorn", 1), elf.inv.obter(2));
        assertEquals(3, elf.inv.tamanho());
        assertEquals(Status.VIVO, elf.getStatus());
        assertEquals(100, elf.getVida(), 0.1);
    }
    
    @Test
    public void AtacarComEspadaTest() {        
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        feanor.atacarComEspada(new Dwarf("Farlum"));
        assertEquals(79, feanor.getVida(), 0.1);
        feanor.atacarComEspada(new Dwarf("Gul"));
        assertEquals(89, feanor.getVida(), 0.1);
    }
    
    @Test
    public void AtacarComEspadaAteMorrerTest() {        
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        Dwarf dwarf = new Dwarf("Gimli");
        int i = 0;
        while(feanor.getStatus() == Status.VIVO) {
            ++i;
            assertEquals(Status.VIVO, feanor.getStatus());
            feanor.atacarComEspada(dwarf);            
        }
        assertEquals(17, i);
        assertEquals(11, feanor.getExperiencia());
    }
    
    @Test
    public void perderGalvornTest() {
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        feanor.perdeItem(new ItemImperdivel("Espada de Galvorn", 1));
        assertEquals(new ItemImperdivel("Espada de Galvorn", 1), feanor.inv.buscarItem("Espada de Galvorn"));
        feanor.inv.getItems().get(2).setQuantidade(0);
        assertEquals(1, feanor.inv.buscarItem("Espada de Galvorn").getQuantidade());
    }
    
    @Test
    public void qtdFlechasTest() {
        ElfoDaLuz elf = new ElfoDaLuz("npc");
        int flechas = elf.getFlecha().getQuantidade();
        Dwarf dwarf = new Dwarf("Oin");
        elf.atirarFlecha(dwarf);
        assertEquals(--flechas, elf.getFlecha().getQuantidade());
    }
    
    @Test
    public void experienciaTest() {
        ElfoDaLuz legolas = new ElfoDaLuz("Legolas");
        int exp = legolas.getExperiencia();
        Dwarf dwarf = new Dwarf("Gloin");
        legolas.atirarFlecha(dwarf);
        assertEquals(++exp, legolas.getExperiencia());
        legolas.atirarFlecha(dwarf);
        assertEquals(++exp, legolas.getExperiencia());
    }
    
    @Test
    public void limiteFlechasTest() {
        ElfoDaLuz elf = new ElfoDaLuz("Fingolfin");
        Dwarf dwarf = new Dwarf("Fili");
        for(int i = 0; i <= 8; i++) elf.atirarFlecha(dwarf);
        assertEquals(0, elf.getFlecha().getQuantidade());
        assertEquals(7, elf.getExperiencia());
        assertEquals(Status.MORTO, elf.getStatus());
    }
    
    @Test
    public void atirarFlechaTest(){
        ElfoDaLuz elf = new ElfoDaLuz("Ciramyr");
        Dwarf dwarf = new Dwarf("Kili");
        elf.atirarFlecha(dwarf);
        assertEquals(6, elf.getFlecha().getQuantidade());
        assertEquals(1, elf.getExperiencia());
        assertEquals(100, dwarf.getVida(), 0.1);
    }
    
    @Test
    public void ganharItemTest() {
        ElfoDaLuz elf = new ElfoDaLuz("npc");        
        elf.ganharItem(new Item("Espada de aço valiriano", 1));
        assertEquals(new Item("Espada de aço valiriano", 1), elf.inv.obter(3));
        assertEquals(4, elf.inv.tamanho());
        elf.ganharItem(new Item("Espada de aço valiriano", 2));
        assertEquals(4, elf.inv.tamanho());
        assertEquals(new Item("Espada de aço valiriano", 3), elf.inv.obter(3));
        elf.ganharItem(new Item("Escudo de madeira", 1));
        assertEquals(5, elf.inv.tamanho());
        assertEquals(new Item("Escudo de madeira", 1), elf.inv.buscarItem("Escudo de madeira"));
    }
    
    @Test
    public void perderItemTest() {
        ElfoDaLuz elf = new ElfoDaLuz("npc"); 
        elf.perdeItem(new Item("Arco", 1));
        assertEquals(2, elf.inv.tamanho());
    }
}