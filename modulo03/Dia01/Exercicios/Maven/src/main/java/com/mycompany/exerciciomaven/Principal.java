package com.mycompany.exerciciomaven;

import org.apache.commons.lang3.*;

/**
 *
 * @author douglas.marques
 */
public class Principal {
    public static void main( String[] args )
    {
    	String s = new String();
    	if(StringUtils.isBlank(s)) System.out.println( "Certo!" );
    	else System.out.println( "Errado!" );
    	
    	s = "Agora entendi como funciona isso aqui.";
    	if(StringUtils.isBlank(s)) System.out.println( "Errado!" );
    	else System.out.println( "Certo!" );
    }
}
