function somarPares(array) {
  return array.reduce((sum, elem, ind) => {
    if (ind % 2 === 0) return sum + elem;
    return sum + 0;
  });
}

function formatarElfos(elfos) {
  elfos.forEach(elfo => {
    elfo.nome = elfo.nome.toUpperCase();
    elfo.temExperiencia = elfo.experiencia;
    elfo.descricao = `${elfo.nome} ${
      elfo.temExperiencia ? "com" : "sem"
    } experiencia e com ${elfo.qtdFlechas} flecha${
      elfo.qtdFlechas !== 1 ? "s" : ""
    }`;
    delete elfo.experiencia;
  });
  return elfos;
}
