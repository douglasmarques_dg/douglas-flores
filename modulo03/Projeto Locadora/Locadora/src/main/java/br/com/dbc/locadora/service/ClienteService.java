package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author douglas.marques
 */
@Service
public class ClienteService extends AbstractCRUDService<Cliente, Long> {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    protected JpaRepository<Cliente, Long> getRepository() {
        return clienteRepository;
    }

    public Page<Cliente> findByNome(Pageable pageable, String nome) {
        return clienteRepository.findByNomeContainingIgnoreCase(pageable, nome);
    }
    
    public Page<Cliente> findByTelefone(Pageable pageable, String telefone) {
        return clienteRepository.findByTelefone(pageable, telefone);
    }
    
    public Page<Cliente> findByEndereco(Pageable pageable, String endereco) {
        return clienteRepository.findByBairroIgnoreCase(pageable, endereco);
    }
}
