package com.exercicio.bancos.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author douglas.marques
 */
@Entity
@Table(name = "BANCO")
@NamedQueries({
    @NamedQuery(name = "Banco.findAll", query = "SELECT b FROM Banco b")
    , @NamedQuery(name = "Banco.findByIdbanco", query = "SELECT b FROM Banco b WHERE b.idbanco = :idbanco")
    , @NamedQuery(name = "Banco.findByNomebanco", query = "SELECT b FROM Banco b WHERE b.nomebanco = :nomebanco")})
public class Banco implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDBANCO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BANCO_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
    private Long idbanco;
    @Column(name = "NOMEBANCO")
    private Long nomebanco;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idbanco")
    private List<Conta> contaList;

    public Banco() {
    }

    public Banco(Long idbanco) {
        this.idbanco = idbanco;
    }
    
    public Banco(Long idbanco, Long nomebanco ) {
        this.idbanco = idbanco;
        this.nomebanco = nomebanco;
    }

    public Long getIdbanco() {
        return idbanco;
    }

    public void setIdbanco(Long idbanco) {
        this.idbanco = idbanco;
    }

    public Long getNomebanco() {
        return nomebanco;
    }

    public void setNomebanco(Long nomebanco) {
        this.nomebanco = nomebanco;
    }

    public List<Conta> getContaList() {
        return contaList;
    }

    public void setContaList(List<Conta> contaList) {
        this.contaList = contaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idbanco != null ? idbanco.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banco)) {
            return false;
        }
        Banco other = (Banco) object;
        if ((this.idbanco == null && other.idbanco != null) || (this.idbanco != null && !this.idbanco.equals(other.idbanco))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exercicio.bancos.model.Banco[ idbanco=" + idbanco + " ]";
    }
}
