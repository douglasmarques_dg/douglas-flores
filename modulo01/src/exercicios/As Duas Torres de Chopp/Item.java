public class Item {
    private String descricao;
    private int quantidade;
    
    public Item(String descricao, int quantidade) {
        this.descricao = descricao;
        this.quantidade = quantidade;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    
    public int getQuantidade() {
        return this.quantidade;
    }
    
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public boolean equals(Object outroItem) {
        if(outroItem == null) return false;
        Item outro = (Item)outroItem;
        return this.descricao.equals(outro.descricao) && this.quantidade == outro.quantidade;
    }
}