package br.com.dbc.minhaseguradora.rest;

import br.com.dbc.minhaseguradora.entity.Apolice;
import br.com.dbc.minhaseguradora.service.ApoliceService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author douglas.marques
 */
@RestController
@RequestMapping("/api/apolice")
public class ApoliceREST {

    @Autowired
    private ApoliceService aS;

    @GetMapping()
    public ResponseEntity<Page<Apolice>> list(Pageable pageable) {
        return ResponseEntity.ok(aS.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Apolice> get(@PathVariable Long id) {
        return aS.findById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Apolice> put(@PathVariable Long id, @RequestBody Apolice apolice) {
        return ResponseEntity.ok(aS.update(id, apolice));
    }

    @PostMapping
    public ResponseEntity<Apolice> post(@RequestBody Apolice apolice) {
        return ResponseEntity.ok(aS.save(apolice));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Apolice> delete(@PathVariable Long id) {
        aS.delete(id);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Error message")
    public void handleError() {

    }
}
