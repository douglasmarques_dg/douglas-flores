package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas.marques
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
