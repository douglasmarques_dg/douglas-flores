package br.com.dbc.locadora.entity;

/**
 *
 * @author douglas.marques
 */
public enum Categoria {
    ANIMACAO, ACAO, AVENTURA
}
