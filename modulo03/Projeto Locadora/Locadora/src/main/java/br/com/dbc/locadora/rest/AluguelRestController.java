package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.DevolucaoDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.service.AluguelService;
import br.com.dbc.locadora.service.MidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author douglas.marques
 */
@RestController
@RequestMapping("/api/aluguel")
public class AluguelRestController extends AbstractRestController<Aluguel, Long, AluguelService> {

    @Autowired
    private AluguelService aluguelService;

    @Override
    protected AluguelService getService() {
        return aluguelService;
    }

    @Autowired
    private MidiaService midiaService;

    @PostMapping("/retirada")
    public ResponseEntity<?> retirar(@RequestBody AluguelDTO input) {
        if (input.getIdCliente() == null || input.getMidias() == null || input.getMidias().isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(aluguelService.save(input));
    }

    @PostMapping("/devolucao")
    public void devolucao(@RequestBody DevolucaoDTO input) {
        if (input.getMidias().isEmpty()) {

        } else {
            midiaService.devolver(input);
        }
    }

    @GetMapping("/devolucao/hoje")
    public ResponseEntity<?> devolucaoHoje(Pageable pageable) {
        return ResponseEntity.ok(aluguelService.buscarHoje(pageable));
    }
}
