describe("Calcula indices pares do Array", function() {
  beforeEach(function() {
    chai.should();
  });

  it("Deve calcular apenas os indices pares do array [1, 2, 3, 4, 5]", function() {
    const array = [1, 2, 3, 4, 5];
    const resultado = somarPares(array);
    resultado.should.equal(9);
  });

  it("Deve calcular apenas os indices pares do array [9, 3, 4, 8, 7, 6, 4, 2, 1, 0]", function() {
    const array = [9, 3, 4, 8, 7, 6, 4, 2, 1, 0];
    const resultado = somarPares(array);
    resultado.should.equal(25);
  });

  it("Deve calcular apenas os indices pares do array de números negativos [-179, -83, -44, -8, -17, -6, -84, 32, -1, -100]", function() {
    const array = [-179, -83, -44, -8, -17, -6, -84, 32, -1, -100];
    const resultado = somarPares(array);
    resultado.should.equal(-325);
  });
});
