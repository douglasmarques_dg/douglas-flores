public class QuadradoImpl implements FormaGeometrica {
    private int x, y;
    
    public QuadradoImpl(int x) {
         setX(x);
    }
    
    public void setX(int x) {
        this.x = x;
        this.y = x;
    }
    
    public void setY(int y) {
        this.y = y;
        this.x = y;
    }
    
    public int calculaArea() {
        return this.x * this.y;
    }
}