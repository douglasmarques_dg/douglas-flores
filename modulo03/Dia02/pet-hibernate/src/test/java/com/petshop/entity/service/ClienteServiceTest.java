package com.petshop.entity.service;

import com.petshop.DAO.ClienteDAO;
import com.petshop.DAO.HibernateUtil;
import com.petshop.entity.Animal;
import com.petshop.entity.Cliente;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javassist.NotFoundException;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author tiago
 */
public class ClienteServiceTest {

    public ClienteServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCreate() {
        System.out.println("create");
        Cliente cliente1 = Cliente.builder()
                .nomecliente("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nomeanimal("Animal 1")
                                .build()))
                .build();
        ClienteService.getInstance().create(cliente1);
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();
        Assert.assertEquals("Quantidade de clientes errada", 1, clientes.size());
        Cliente result = clientes.stream().findAny().get();
        assertEquals("Cliente diferente", cliente1.getId(), result.getId());
        assertEquals("Quantidade animais diferente",
                cliente1.getAnimalList().size(),
                result.getAnimalList().size());
        assertEquals("Animais diferente",
                cliente1.getAnimalList().stream().findAny().get().getId(),
                result.getAnimalList().stream().findAny().get().getId());
        session.close();
    }
    
      @Test
    public void testCreateMocked() {
        System.out.println("Criar Cliente com Mock");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).saveOrUpdate(ArgumentMatchers.any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);
        Cliente cliente = Cliente.builder()
                .nomecliente("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nomeanimal("Animal 1")
                                .build()))
                .build();
        clienteService.create(cliente);
        verify(daoMock, times(1)).saveOrUpdate(any());
    }

    @Test
    public void testFindOneMocked() {
        System.out.println("Testar Find One");
        Cliente cliente = Cliente.builder()
                .id(1L)
                .nomecliente("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nomeanimal("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);
        Mockito.when(daoMock.findById(1L)).thenReturn(cliente);        
        clienteService.findOne(cliente.getId());
        verify(daoMock, times(1)).findById(any());
    }
    
    @Test
    public void testFindAllMocked() {
        System.out.println("Testar Find All");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);
        List<Cliente> clientes = new ArrayList<>();
        Mockito.when(daoMock.findAll()).thenReturn(clientes);
        clienteService.findAll();
        verify(daoMock, times(1)).findAll();
    }

    @Test
    public void testUpdateMocked() {
        System.out.println("Atualizar Cliente com Mock");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).saveOrUpdate(ArgumentMatchers.any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);
        Cliente cliente = Cliente.builder()
                .id(1L)
                .nomecliente("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nomeanimal("Animal 1")
                                .build()))
                .build();
        clienteService.update(cliente);
        verify(daoMock, times(1)).saveOrUpdate(any());
    }

    @Test
    public void testDeleteMocked() throws NotFoundException {
        System.out.println("Deletar Cliente com Mock");       
        Cliente cliente = Cliente.builder()
                .id(1L)
                .nomecliente("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nomeanimal("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).delete(ArgumentMatchers.any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(daoMock.findById(1L)).thenReturn(cliente);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);        
        clienteService.delete(cliente.getId());
        verify(daoMock, times(1)).delete(cliente);
    }
}
