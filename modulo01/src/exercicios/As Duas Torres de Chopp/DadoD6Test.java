import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void sortearTest() {
        DadoD6 D6 = new DadoD6();
        int x = D6.sortear();
        assertTrue(x < 7 && x > 0);
    }
}