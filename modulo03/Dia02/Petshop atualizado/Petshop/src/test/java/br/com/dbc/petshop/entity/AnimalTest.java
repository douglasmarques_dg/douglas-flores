package br.com.dbc.petshop.entity;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author tiago
 */
public class AnimalTest {

    public AnimalTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        PersistenceUtils.getEm();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class Animal.     
    @Test
    public void testFindAll() {
        System.out.println("findAll Animal");
        Animal instance = new Animal();
        List<Animal> result = instance.findAll();
        assertEquals(0, result.size());
        Animal a = result.get(0);
        assertEquals("Thor", a.getNome());
        assertEquals(SexoType.M, a.getSexo());
        assertEquals("Odin", a.getIdCliente().getNome());
    }
    */
}
