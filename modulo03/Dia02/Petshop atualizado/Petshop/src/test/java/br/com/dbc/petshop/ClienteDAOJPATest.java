package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.util.List;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author douglas.marques
 */
public class ClienteDAOJPATest {

    public ClienteDAOJPATest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        PersistenceUtils.beginTransaction();
        PersistenceUtils.getEm().createNativeQuery("delete from animal").executeUpdate();
        PersistenceUtils.getEm().createNativeQuery("delete from cliente").executeUpdate();
        PersistenceUtils.getEm().getTransaction().commit();
    }

    /**
     * Test of persist method, of class ClienteDAOJPA. JPA
     */
    @Test
    public void testPersistir() {
        System.out.println("persistir");

        ClienteDAOJPA instanceCliente = new ClienteDAOJPA();

        for (int i = 0; i <= 9; i++) {
            Cliente cliente = new Cliente(null, String.format("Cliente {i}", i), SexoType.F, String.format("Profissao {i}", i));
            instanceCliente.persistir(cliente);
        }

        List<Cliente> resultado = instanceCliente.findAll();
        assertEquals(10, resultado.size());
    }

    /**
     * Test of remover method, of class ClienteDAOJPA. JPA
     */
    @Test
    public void testRemover() {
        System.out.println("remover");

        ClienteDAOJPA instanceCliente = new ClienteDAOJPA();

        Cliente cliente = new Cliente(null, "Cliente para remover", SexoType.M, "Profissao teste");
        instanceCliente.persistir(cliente);

        List<Cliente> resultado = instanceCliente.findAll();
        assertEquals(1, resultado.size());

        instanceCliente.remover(cliente);

        resultado = instanceCliente.findAll();
        assertEquals(0, resultado.size());
    }

    /**
     * Test of merge method, of class ClienteDAOJPA. JPA
     */
    @Test
    public void testMerge() {
        System.out.println("merge");

        ClienteDAOJPA instanceCliente = new ClienteDAOJPA();

        Cliente c = new Cliente(null, "Cliente teste", SexoType.F, "Profissao teste");
        instanceCliente.merge(c);

        List<Cliente> resultado = instanceCliente.findAll();
        assertEquals(1, resultado.size());

        c.setNome("Cliente atualizado");
        instanceCliente.merge(c);

        resultado = instanceCliente.findAll();
        assertEquals(1, resultado.size());
    }
}
