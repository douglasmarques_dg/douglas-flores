package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.DevolucaoDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.AluguelRepository;
import br.com.dbc.locadora.repository.ClienteRepository;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import static java.time.format.DateTimeFormatter.ofPattern;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author douglas.marques
 */
public class AluguelRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private AluguelRestController aluguelRestController;

    @Autowired
    private AluguelRepository aluguelRespository;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    protected AbstractRestController getController() {
        return aluguelRestController;
    }

    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
        aluguelRespository.deleteAll();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void retirarAluguelTest() throws Exception {
        Cliente cliente = Cliente.builder()
                .nome("nome")
                .telefone("9999")
                .rua("Rua")
                .bairro("Bairro")
                .cidade("Cidade")
                .estado("Estado")
                .build();
        clienteRepository.save(cliente);

        Filme filme = Filme.builder()
                .titulo("Rei Leão")
                .lancamento(LocalDate.of(1994, 07, 06))
                .categoria(Categoria.AVENTURA)
                .build();
        filmeRepository.save(filme);

        Midia midia = Midia.builder()
                .tipo(MidiaType.VHS).
                idFilme(filme)
                .build();
        midiaRepository.save(midia);

        ValorMidia valorMidia = ValorMidia.builder()
                .valor(3.9)
                .inicioVigencia(LocalDateTime.now())
                .idMidia(midia)
                .build();
        valorMidiaRepository.save(valorMidia);

        ArrayList<Long> midias = new ArrayList<>();
        midias.add(midia.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(0.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.idCliente").value(cliente));
    }

    @Test()
    public void devolucaoHojeTeste() throws Exception {
        Cliente cliente = Cliente.builder()
                .nome("nome")
                .telefone("9999")
                .rua("Rua")
                .bairro("Bairro")
                .cidade("Cidade")
                .estado("Estado")
                .build();
        clienteRepository.save(cliente);

        Filme filme = Filme.builder()
                .titulo("Rei Leão")
                .lancamento(LocalDate.of(1994, 07, 06))
                .categoria(Categoria.AVENTURA)
                .build();
        filmeRepository.save(filme);

        Midia midia = Midia.builder()
                .tipo(MidiaType.VHS).
                idFilme(filme)
                .build();
        midiaRepository.save(midia);

        ValorMidia valorMidia = ValorMidia.builder()
                .valor(3.9)
                .inicioVigencia(LocalDateTime.now())
                .idMidia(midia)
                .build();
        valorMidiaRepository.save(valorMidia);

        ArrayList<Long> midias = new ArrayList<>();
        midias.add(midia.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(0.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.idCliente").value(cliente));

        List<Aluguel> alugueis = aluguelRespository.findAll();

        Assert.assertEquals(1, alugueis.size());
        Assert.assertEquals(cliente, alugueis.get(0).getIdCliente());

        if (LocalTime.now().getHour() > 16) {
            return;
        }
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/aluguel/devolucao/hoje")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].multa").value(0.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].idCliente").value(cliente));
    }

    @Test
    public void devolucaoTest() throws Exception {
        Cliente cliente = Cliente.builder()
                .nome("nome")
                .telefone("9999")
                .rua("Rua")
                .bairro("Bairro")
                .cidade("Cidade")
                .estado("Estado")
                .build();
        clienteRepository.save(cliente);

        Filme filme = Filme.builder()
                .titulo("Rei Leão")
                .lancamento(LocalDate.of(1994, 07, 06))
                .categoria(Categoria.AVENTURA)
                .build();
        filmeRepository.save(filme);

        Midia midia = Midia.builder()
                .tipo(MidiaType.VHS).
                idFilme(filme)
                .build();
        midiaRepository.save(midia);

        ValorMidia valorMidia = ValorMidia.builder()
                .valor(3.9)
                .inicioVigencia(LocalDateTime.now())
                .idMidia(midia)
                .build();
        valorMidiaRepository.save(valorMidia);

        ArrayList<Long> midias = new ArrayList<>();
        midias.add(midia.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(0.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.idCliente").value(cliente));
        List<Aluguel> alugados = aluguelRespository.findAll();
        Assert.assertEquals(null, alugados.get(0).getDevolucao());
        
        DevolucaoDTO devolucao = DevolucaoDTO.builder()
                .midias(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(devolucao)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        alugados = aluguelRespository.findAll();
        Assert.assertEquals(LocalDateTime.now().format(ofPattern("dd/MM/yyyy")), alugados.get(0).getDevolucao().format(ofPattern("dd/MM/yyyy")));
    }
}
