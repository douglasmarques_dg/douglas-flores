public class NovoPrograma {
    public static void main(String[] args) {
        int entrada = 2, x = 5, y = 10;
        FormaGeometrica formaGeometrica = FormaGeometricaFactory.criar(entrada, x, y);
        System.out.println(formaGeometrica.calculaArea());
    }
}