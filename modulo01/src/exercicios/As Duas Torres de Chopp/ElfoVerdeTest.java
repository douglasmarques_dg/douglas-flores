import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void criarElfoVerdeTest() {
        ElfoVerde elf = new ElfoVerde("npc");
        assertEquals("npc", elf.getNome());
        assertEquals(new Item("Arco", 1), elf.inv.obter(0));
        assertEquals(new Item("Flecha", 7), elf.inv.obter(1));
        assertEquals(2, elf.inv.tamanho());
        assertEquals(Status.VIVO, elf.getStatus());
        assertEquals(100, elf.getVida(), 0.1);
    }
    
    @Test
    public void qtdFlechasTest() {
        ElfoVerde elf = new ElfoVerde("npc");
        int flechas = elf.getFlecha().getQuantidade();
        Dwarf dwarf = new Dwarf("Oin");
        elf.atirarFlecha(dwarf);
        assertEquals(--flechas, elf.getFlecha().getQuantidade());
    }
    
    @Test
    public void experienciaTest() {
        ElfoVerde legolas = new ElfoVerde("Legolas");
        int exp = legolas.getExperiencia();
        Dwarf dwarf = new Dwarf("Gloin");
        legolas.atirarFlecha(dwarf);
        assertEquals(exp+=2, legolas.getExperiencia());
        legolas.atirarFlecha(dwarf);
        assertEquals(exp+=2, legolas.getExperiencia());
    }
    
    @Test
    public void limiteFlechasTest() {
        ElfoVerde elf = new ElfoVerde("Fingolfin");
        Dwarf dwarf = new Dwarf("Fili");
        for(int i = 0; i <= 8; i++) elf.atirarFlecha(dwarf);
        assertEquals(0, elf.getFlecha().getQuantidade());
        assertEquals(14, elf.getExperiencia());
    }
    
    @Test
    public void atirarFlechaTest(){
        ElfoVerde elf = new ElfoVerde("Ciramyr");
        Dwarf dwarf = new Dwarf("Kili");
        elf.atirarFlecha(dwarf);
        assertEquals(6, elf.getFlecha().getQuantidade());
        assertEquals(2, elf.getExperiencia());
        assertEquals(100, dwarf.getVida(), 0.1);
    }
    
    @Test
    public void ganharItemTest() {
        ElfoVerde elf = new ElfoVerde("npc");        
        elf.ganharItem(new Item("Espada de aço valiriano", 1));
        assertEquals(new Item("Espada de aço valiriano", 1), elf.inv.obter(2));
        assertEquals(3, elf.inv.tamanho());
        elf.ganharItem(new Item("Espada de aço valiriano", 2));
        assertEquals(3, elf.inv.tamanho());
        assertEquals(new Item("Espada de aço valiriano", 3), elf.inv.obter(2));
        elf.ganharItem(new Item("Escudo de madeira", 1));
        assertEquals(3, elf.inv.tamanho());
        assertEquals(null, elf.inv.buscarItem("Escudo de madeira"));
    }
}