const meuH2 = document.getElementById("tituloPagina");
const buttom = document.querySelector("#limpa");

carregarEventos();

function carregarEventos() {
  buttom.addEventListener("click", limparNome);
  document.addEventListener("DOMContentLoaded", principal);
}

function principal() {
  verificarLimite();
  if (verificarNome()) {
    renderizarNomeArmazenadoNaTela();
  } else {
    perguntarNome();
  }
}

function perguntarNome() {
  const nome = prompt("Qual seu nome?");
  configurarLS(nome);
  renderizarNomeArmazenadoNaTela();
}

function configurarLS(nome) {
  localStorage.nome = nome;
  verificarLimite();
  num = localStorage.getItem("numero");
  if (verificarNumero()) localStorage.setItem("numero", parseInt(num) + 1);
  else localStorage.numero = 1;
}

function renderizarNomeArmazenadoNaTela() {
  meuH2.innerText = localStorage.nome;
}

function limparNome() {
  localStorage.removeItem("nome");
  verificarLimite();
  recarregarPagina();
}

function recarregarPagina() {
  location.reload();
}

function verificarNome() {
  return localStorage.nome && localStorage.nome.length > 0;
}

function verificarNumero() {
  return localStorage.numero && localStorage.numero > 0;
}

function verificarLimite() {
  if (parseInt(localStorage.getItem("numero")) >= 4) desabilitarBotao();
}

function desabilitarBotao() {
  buttom.disabled = true;
  localStorage.disable = true;
}
