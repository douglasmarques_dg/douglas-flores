import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void criarDwarfTest() {
        Dwarf dwarf = new Dwarf("Bombur");
        assertEquals("Bombur", dwarf.getNome());
        assertEquals(null, dwarf.inv.obter(0));
        assertEquals(Status.VIVO, dwarf.getStatus());
        assertEquals(110, dwarf.getVida(), 0.1);
    } 
    
    @Test
    public void dwarfPerdeVidaTest() {
        Dwarf dwarf = new Dwarf("Dori");
        dwarf.receberDano();
        assertEquals(100, dwarf.getVida(), 0.1);
    }
    
    @Test
    public void dwarfStatusTest() {
        Dwarf dwarf = new Dwarf("Nori");
        Status status = Status.VIVO;
        assertEquals(status, dwarf.getStatus());
    }
    
    @Test
    public void dwarfConfereVidaTest() {
        Dwarf dwarf = new Dwarf("Ori");
        Status status = Status.MORTO;
        for(int i = 0; i <= 11; i++) dwarf.receberDano();
        assertEquals(0, dwarf.getVida(), 0.1);
        assertEquals(status, dwarf.getStatus());
    }
    
    @Test
    public void ganharItemTest() {
        Dwarf dwarf = new Dwarf("npc");        
        dwarf.ganharItem(new Item("Espada de aço valiriano", 1));
        assertEquals(new Item("Espada de aço valiriano", 1), dwarf.inv.obter(0));
        assertEquals(1, dwarf.inv.tamanho());  
        dwarf.ganharItem(new Item("Espada de aço valiriano", 2));
        assertEquals(new Item("Espada de aço valiriano", 3), dwarf.inv.obter(0));
        dwarf.ganharItem(new Item("Escudo de madeira", 1));
        assertEquals(new Item("Escudo de madeira", 1), dwarf.inv.buscarItem("Escudo de madeira"));
    }
}