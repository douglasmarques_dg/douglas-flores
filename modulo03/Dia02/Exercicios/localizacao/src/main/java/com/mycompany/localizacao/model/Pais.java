package com.mycompany.localizacao.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author douglas.marques
 */
@Entity
@Table(name = "PAIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pais.findAll", query = "SELECT p FROM Pais p")
    , @NamedQuery(name = "Pais.findByIdpais", query = "SELECT p FROM Pais p WHERE p.idpais = :idpais")
    , @NamedQuery(name = "Pais.findByNamepais", query = "SELECT p FROM Pais p WHERE p.namepais = :namepais")
    , @NamedQuery(name = "Pais.findBySiglapais", query = "SELECT p FROM Pais p WHERE p.siglapais = :siglapais")})
public class Pais implements Serializable {

    private static final Long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDPAIS")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAIS_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "PAIS_SEQ", sequenceName = "PAIS_SEQ")
    private Long idpais;
    @Basic(optional = false)
    @Column(name = "NAMEPAIS")
    private String namepais;
    @Column(name = "SIGLAPAIS")
    private String siglapais;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpais")
    private List<Estado> estadoList;

    public Pais() {
    }

    public Pais(Long idpais) {
        this.idpais = idpais;
    }

    public Pais(Long idpais, String namepais, String siglaPais) {
        this.idpais = idpais;
        this.namepais = namepais;
        this.siglapais = siglaPais;
    }

    public Long getIdpais() {
        return idpais;
    }

    public void setIdpais(Long idpais) {
        this.idpais = idpais;
    }

    public String getNamepais() {
        return namepais;
    }

    public void setNamepais(String namepais) {
        this.namepais = namepais;
    }

    public String getSiglapais() {
        return siglapais;
    }

    public void setSiglapais(String siglapais) {
        this.siglapais = siglapais;
    }

    @XmlTransient
    public List<Estado> getEstadoList() {
        return estadoList;
    }

    public void setEstadoList(List<Estado> estadoList) {
        this.estadoList = estadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpais != null ? idpais.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pais)) {
            return false;
        }
        Pais other = (Pais) object;
        if ((this.idpais == null && other.idpais != null) || (this.idpais != null && !this.idpais.equals(other.idpais))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.localizacao.model.Pais[ idpais=" + idpais + " ]";
    }
}
