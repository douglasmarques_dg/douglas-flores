/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exercicio.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dougl
 */
@Entity
@Table(name = "NOTA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nota.findAll", query = "SELECT n FROM Nota n")
    , @NamedQuery(name = "Nota.findByIdnota", query = "SELECT n FROM Nota n WHERE n.idnota = :idnota")
    , @NamedQuery(name = "Nota.findByDescricaonota", query = "SELECT n FROM Nota n WHERE n.descricaonota = :descricaonota")})
public class Nota implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDNOTA")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NOTA_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "NOTA_SEQ", sequenceName = "NOTA_SEQ")
    private Long idnota;
    @Column(name = "DESCRICAONOTA")
    private String descricaonota;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idnota")
    private List<Itemnota> itemnotaList;

    public Nota() {
    }

    public Nota(Long idnota) {
        this.idnota = idnota;
    }

    public Long getIdnota() {
        return idnota;
    }

    public void setIdnota(Long idnota) {
        this.idnota = idnota;
    }

    public String getDescricaonota() {
        return descricaonota;
    }

    public void setDescricaonota(String descricaonota) {
        this.descricaonota = descricaonota;
    }

    @XmlTransient
    public List<Itemnota> getItemnotaList() {
        return itemnotaList;
    }

    public void setItemnotaList(List<Itemnota> itemnotaList) {
        this.itemnotaList = itemnotaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idnota != null ? idnota.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nota)) {
            return false;
        }
        Nota other = (Nota) object;
        if ((this.idnota == null && other.idnota != null) || (this.idnota != null && !this.idnota.equals(other.idnota))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exercicio.model.Nota[ idnota=" + idnota + " ]";
    }
}
