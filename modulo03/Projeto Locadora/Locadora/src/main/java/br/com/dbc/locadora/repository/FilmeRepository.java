package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas.marques
 */
public interface FilmeRepository extends JpaRepository<Filme, Long> {

    public Page<Filme> findByCategoria(Pageable pageable, Categoria categoria);
    
    public Page<Filme> findByTituloContainingIgnoreCase(Pageable pageable, String titulo);
    
    public Page<Filme> findByLancamento(Pageable pageable, LocalDate lancamento);
}
