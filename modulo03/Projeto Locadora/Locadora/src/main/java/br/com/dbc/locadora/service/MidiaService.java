package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.DevolucaoDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas.marques
 */
@Service
public class MidiaService extends AbstractCRUDService<Midia, Long> {

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Autowired
    private AluguelService aluguelService;

    @Override
    protected JpaRepository<Midia, Long> getRepository() {
        return midiaRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void saveMidias(Filme filme, MidiaDTO dto) {
        for (int i = 0; i < dto.getQuantidade(); i++) {
            Midia midia = midiaRepository.save(
                    Midia.builder()
                            .tipo(dto.getTipo())
                            .idAluguel(null)
                            .idFilme(filme)
                            .build());
            valorMidiaService.savePreco(midia, dto.getValor());
        }
    }

    public int countByTipo(MidiaType tipo) {
        return midiaRepository.countByTipo(tipo);
    }
    
    public int countByTipoFilme(Long id, MidiaType tipo) {
        return midiaRepository.countByTipoAndIdFilmeId(tipo, id);
    }

    public void edit(Midia midia, Aluguel aluguel) {
        midia.setIdAluguel(aluguel);
        midiaRepository.save(midia);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void devolver(DevolucaoDTO midias) {
        midias.getMidias().forEach((midia) -> {
            Midia m = midiaRepository.findById(midia).orElseGet(null);
            Aluguel a = m.getIdAluguel();
            a.setDevolucao(LocalDateTime.now());
            if (a.getDevolucao().getDayOfMonth() != a.getPrevisao().getDayOfMonth() && a.getDevolucao().getHour() >= 16) {
                a.setMulta(valorMidiaService.findByMidia(m));
            }
            this.edit(m, null);
            aluguelService.save(a);
        });
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateMidiaDTO(MidiaDTO dto, Filme filme) {
        List<Midia> midiasAtuais = midiaRepository.findByIdFilme(filme).stream()
                .filter((midia) -> {
                    return midia.getTipo().equals(dto.getTipo());
                }).collect(Collectors.toList());
        int diferenca = dto.getQuantidade() - midiasAtuais.size();
        if (diferenca < 0) {
            deletarMidias(-1 * diferenca, midiasAtuais);
        }
        midiasAtuais.forEach((m) -> {
            valorMidiaService.updateByIdMidia(m, dto.getValor());
        });
        if (diferenca > 0) {
            dto.setQuantidade(diferenca);
            saveMidias(filme, dto);
        }
    }
    
    public List<Midia> findByIdFilme(Long idFilme) {
        return midiaRepository.findByIdFilmeId(idFilme);
    }
    
    public List<Midia> findByIdFilmeSemAluguel(Long idFilme) {
        return midiaRepository.findByIdFilmeIdAndIdAluguelIsNull(idFilme);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<Midia> deletarMidias(int tam, List<Midia> midias) {
        List<Midia> midiasLivres = midias.stream().filter(m -> m.getIdAluguel() == null).collect(Collectors.toList());
        for (int i = 0; i < tam; i++) {
            valorMidiaService.deleteByIdMidiaId(midiasLivres.get(0));
            delete(midiasLivres.get(0).getId());
            midiasLivres.remove(0);
        }
        return midiasLivres;
    }
}
