package com.petshop.entity.service;

import com.petshop.DAO.ClienteDAO;
import com.petshop.entity.Cliente;

public class ClienteService extends GenericService< Cliente, Long, ClienteDAO > {
    
    private static ClienteService instance;
    
    static{
        instance = new ClienteService();
    }
    
    public static ClienteService getInstance(){
        return instance;
    }
        
    @Override
    protected ClienteDAO getDao() {
        return ClienteDAO.getInstance();
    }
}