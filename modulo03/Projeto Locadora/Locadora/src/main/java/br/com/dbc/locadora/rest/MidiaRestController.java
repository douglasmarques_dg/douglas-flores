package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.service.MidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author douglas.marques
 */
@RestController
@RequestMapping("/api/midia")
public class MidiaRestController extends AbstractRestController<Midia, Long, MidiaService> {

    @Autowired
    private MidiaService midiaService;

    @Override
    protected MidiaService getService() {
        return midiaService;
    }

    @GetMapping("/contar/{tipo}")
    public ResponseEntity<?> get(@PathVariable MidiaType tipo) {
        return ResponseEntity.ok(midiaService.countByTipo(tipo));
    }
}
