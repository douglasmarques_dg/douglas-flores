import java.util.ArrayList;

public class EstatisticasInventario {
    private Inventario inv;
    
    public EstatisticasInventario(Inventario inv) {
        this.inv = inv;
    }
    
    public int calcularMedia() {
        if(inv == null || inv.getItems() == null) return 0;
        int quantidade = 0;
        ArrayList<Item> itens = inv.getItems();
        for(Item i : itens) quantidade += i.getQuantidade();
        return (int) Math.floor((quantidade / itens.size()));
    }
    
    public ArrayList<Item> bubbleSort() {
        ArrayList<Item> itens = this.inv.getItems();
        ArrayList<Item> itensOrd = new ArrayList<>(itens.size());
        for(Item i : itens) itensOrd.add(i);
        for(int i = 1; i < itensOrd.size(); i++)
            for(int j = 1; j <= itensOrd.size()-i; j++)
                if(itensOrd.get(j-1).getQuantidade() > itensOrd.get(j).getQuantidade()) {
                    Item temp = itensOrd.get(j-1);
                    itensOrd.set(j-1, itensOrd.get(j));
                    itensOrd.set(j, temp);
                }
        return itensOrd;
    }
    
    public ArrayList<Item> inverter(ArrayList<Item> itens) {
        if(itens == null || itens.size() == 0) return null;
        ArrayList<Item> itensInv = new ArrayList<>(itens.size());
        for(int i = itens.size()-1; i >= 0; i--) itensInv.add(itens.get(i));
        return itensInv;
    }
    
    public ArrayList<Item> ordenar(TipoOrdenacao tipo) {
        if(this.inv == null || this.inv.tamanho() == 0) return null;
        ArrayList<Item> ord = this.bubbleSort();
        return (tipo == tipo.ASC) ? ord : this.inverter(ord);
    }
    
    public int calcularMediana() {
        ArrayList<Item> itensOrd = this.bubbleSort();
        int pos = itensOrd.size()/2;
        if(itensOrd.size() % 2 != 0) 
            return itensOrd.get(pos).getQuantidade();
        else {
            int soma = itensOrd.get(pos).getQuantidade() + itensOrd.get(pos-1).getQuantidade();
            return (int) soma/2;
        }
    }
    
    public int qtdItensAcimaDaMedia() {
        int media = calcularMedia(), acimaMedia = 0;
        for(Item i : this.inv.getItems())
            if(i.getQuantidade() > media) acimaMedia++;
        return acimaMedia;
    }
}