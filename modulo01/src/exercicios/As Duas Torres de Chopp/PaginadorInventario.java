import java.util.List;

public class PaginadorInventario {
    private Inventario inv;
    private int index;
    
    {
        this.index = 0;
    }
    
    public PaginadorInventario(Inventario inv) {
        this.inv = inv;
    }
    
    public int getIndex() {
        return this.index;
    }
    
    public boolean pular(int pule) {
        if(pule < 0 || pule > this.inv.tamanho()) return false;
        if((this.index + pule) > this.inv.tamanho()) return false;
        this.index += pule;
        return true;
    }
    
    public List<Item> limitar(int limite) {
        if(limite > this.inv.tamanho()) limite = this.inv.tamanho();
        return this.inv.getItems().subList(this.index, this.inv.tamanho());
    }
}