const contador = document.querySelector("#clock");

function principal() {
  let tempo = 30;
  contador.value = tempo;

  const timer = setInterval(acabandoTempo, 1000);

  function acabandoTempo() {
    contador.value = --tempo;
    if (tempo <= 10) contador.style.color = "red";
    if (tempo === 0) {
      alert("T E M P O  E N C E R R A D O");
      clearInterval(timer);
      document.body.style.backgroundImage =
        "url('https://static.wixstatic.com/media/84ee6a_dad7a15e9bcf4127aa2dda92192dbdb8~mv2.gif')";
    }
  }
}

window.addEventListener("load", principal());
