package com.exercicio.bancos.DAO;

import com.exercicio.bancos.model.Banco;
import com.exercicio.bancos.util.PersistenceUtil;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author douglas.marques
 */
public class BancoDAOTest {
    
    public BancoDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        PersistenceUtil.beginTransaction();
        PersistenceUtil.getEm().createNativeQuery("delete from banco").executeUpdate();
        PersistenceUtil.getEm().getTransaction().commit();
    }

    /**
     * Test of getById method, of class BancoDAO.
     */
    @Test
    public void testGetById() {
        System.out.println("getById");
        
        BancoDAO instance = new BancoDAO();
        
        long id = 0;
        
        Banco expResult = null;
        Banco result = instance.getById(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of findAll method, of class BancoDAO.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        BancoDAO instance = new BancoDAO();
        List<Banco> result = instance.findAll();
        assertEquals(0, result.size());
    }

    /**
     * Test of persistir method, of class BancoDAO.
     */
    @Test
    public void testPersistir() {
        System.out.println("persistir");
        
        BancoDAO instance = new BancoDAO();
        
        List<Banco> result = instance.findAll();
        assertEquals(0, result.size());
        
        long teste = 9;
        Banco banco = new Banco(null, teste);
        
        instance.persistir(banco);
        
        result = instance.findAll();
        assertEquals(1, result.size());
    }

    /**
     * Test of merge method, of class BancoDAO.
     */
    @Test
    public void testMerge() {
        System.out.println("merge");
        
        BancoDAO instance = new BancoDAO();
        
        List<Banco> result = instance.findAll();
        assertEquals(0, result.size());
        
        long teste = 14;
        Banco banco = new Banco(null, teste);
        
        instance.merge(banco);
        
        result = instance.findAll();
        assertEquals(1, result.size());
        
        teste = 99;
        banco.setNomebanco(teste);
        
        instance.merge(banco);
        
        result = instance.findAll();
        assertEquals(1, result.size());
        assertEquals(1L, teste, result.get(0).getNomebanco());
    }

    /**
     * Test of remover method, of class BancoDAO.
     */
    @Test
    public void testRemover() {
        System.out.println("remover");
        
        BancoDAO instance = new BancoDAO();
        
        List<Banco> result = instance.findAll();
        assertEquals(0, result.size());
        
        long teste = 9;
        Banco banco = new Banco(null, teste);
        
        instance.persistir(banco);
        
        result = instance.findAll();
        assertEquals(1, result.size());
        
        instance.remover(banco);
        
        result = instance.findAll();
        assertEquals(0, result.size());
    }

    /**
     * Test of removeById method, of class BancoDAO.
     */
    @Test
    public void testRemoveById() {
        System.out.println("removeById");
        BancoDAO instance = new BancoDAO();
        
        List<Banco> result = instance.findAll();
        assertEquals(0, result.size());
        
        long teste = 9;
        Banco banco = new Banco(null, teste);
        
        instance.persistir(banco);
        
        result = instance.findAll();
        assertEquals(1, result.size());
        
        instance.removeById(banco.getIdbanco());
        
        result = instance.findAll();
        assertEquals(0, result.size());
    }
}
