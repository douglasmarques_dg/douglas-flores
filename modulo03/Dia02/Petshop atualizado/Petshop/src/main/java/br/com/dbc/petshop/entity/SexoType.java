package br.com.dbc.petshop.entity;

/**
 *
 * @author tiago
 */
public enum SexoType {
    M, F;
}
