package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author douglas.marques
 */
public class ClienteDAOHIBERNATETest {

    public ClienteDAOHIBERNATETest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        PersistenceUtils.beginTransaction();
        PersistenceUtils.getEm().createNativeQuery("delete from animal").executeUpdate();
        PersistenceUtils.getEm().createNativeQuery("delete from cliente").executeUpdate();
        PersistenceUtils.getEm().getTransaction().commit();
    }

    /**
     * Test of findAll method, of class AnimalDAOHIBERNATE.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");

        ClienteDAOHIBERNATE instance = new ClienteDAOHIBERNATE();

        List<Cliente> result = instance.findAll();
        assertEquals(0, result.size());
    }

    /**
     * Test of findName method, of class AnimalDAOHIBERNATE.
     */
    @Test
    public void testFindName() {
        System.out.println("findName");

        String nome = "JOAO";
        ClienteDAOHIBERNATE instance = new ClienteDAOHIBERNATE();

        List<Cliente> result = instance.findName(nome);
        assertEquals(0, result.size());
    }

    /**
     * Test of saveOrUpdate method, of class AnimalDAOHIBERNATE.
     */
    @Test
    public void testSaveOrUpdate() {
        System.out.println("saveOrUpdate");

        ClienteDAOHIBERNATE instance = new ClienteDAOHIBERNATE();

        Cliente cliente = new Cliente(null, "Cliente para teste", SexoType.M, "Profissao teste");
        instance.saveOrUpdate(cliente);
        List<Cliente> result = instance.findAll();
        assertEquals(1, result.size());

        cliente.setNome("Cliente atualizado");
        instance.saveOrUpdate(cliente);
        result = instance.findAll();
        assertEquals(1, result.size());
    }

    /**
     * Test of delete method, of class AnimalDAOHIBERNATE.
     */
    @Test
    public void testDelete() {
        System.out.println("delete");

        ClienteDAOHIBERNATE instance = new ClienteDAOHIBERNATE();

        Cliente cliente = new Cliente(null, "Cliente para teste", SexoType.M, "Profissao teste");
        instance.saveOrUpdate(cliente);

        List<Cliente> result = instance.findAll();
        assertEquals(1, result.size());

        instance.delete(cliente);

        result = instance.findAll();
        assertEquals(0, result.size());
    }
}
