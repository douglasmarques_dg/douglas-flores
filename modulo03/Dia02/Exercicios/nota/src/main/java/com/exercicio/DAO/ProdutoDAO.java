package com.exercicio.DAO;

import com.exercicio.model.Produto;
import com.exercicio.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;

public class ProdutoDAO {

    private static ProdutoDAO instance;
    protected EntityManager entityManager;

    public static ProdutoDAO getInstance() {
        if (instance == null) {
            instance = new ProdutoDAO();
        }
        return instance;
    }

    public ProdutoDAO() {
        entityManager = PersistenceUtil.getEm();
    }

    public Produto getById(final long id) {
        return entityManager.find(Produto.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Produto> findAll() {
        return entityManager.createQuery("FROM " + Produto.class.getName()).getResultList();
    }

    public void persistir(Produto produto) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(produto);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Produto produto) {
        try {
            entityManager.getTransaction().begin();
            if (produto.getIdproduto() == null) {
                entityManager.persist(produto);
            } else {
                produto = entityManager.merge(produto);
            }
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remover(Produto produto) {
        try {
            entityManager.getTransaction().begin();
            produto = entityManager.find(Produto.class, produto.getIdproduto());
            entityManager.remove(produto);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final long id) {
        try {
            Produto produto = getById(id);
            remover(produto);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
