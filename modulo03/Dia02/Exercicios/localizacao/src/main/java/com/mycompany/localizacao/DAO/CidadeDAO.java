package com.mycompany.localizacao.DAO;

import com.mycompany.localizacao.model.Cidade;
import com.mycompany.localizacao.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;

public class CidadeDAO {

    private static CidadeDAO instance;
    protected EntityManager entityManager;

    public static CidadeDAO getInstance() {
        if (instance == null) {
            instance = new CidadeDAO();
        }
        return instance;
    }

    public CidadeDAO() {
        entityManager = PersistenceUtil.getEm();
    }

    public Cidade getById(final long id) {
        return entityManager.find(Cidade.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Cidade> findAll() {
        return entityManager.createQuery("FROM " + Cidade.class.getName()).getResultList();
    }

    public void persistir(Cidade cidade) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(cidade);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Cidade cidade) {
        try {
            entityManager.getTransaction().begin();
            if (cidade.getIdcidade() == null) {
                entityManager.persist(cidade);
            } else {
                cidade = entityManager.merge(cidade);
            }
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remover(Cidade cidade) {
        try {
            entityManager.getTransaction().begin();
            cidade = entityManager.find(Cidade.class, cidade.getIdcidade());
            entityManager.remove(cidade);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final long id) {
        try {
            Cidade cidade = getById(id);
            remover(cidade);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
