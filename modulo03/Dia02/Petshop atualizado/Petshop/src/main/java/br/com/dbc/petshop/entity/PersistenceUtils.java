package br.com.dbc.petshop.entity;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author douglas.marques
 */
public class PersistenceUtils {

    static EntityManager em;

    static {
        em = Persistence.createEntityManagerFactory("petshop").createEntityManager();
    }

    public static EntityManager getEm() {
        return em;
    }

    public static void beginTransaction() {
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
    }
}
