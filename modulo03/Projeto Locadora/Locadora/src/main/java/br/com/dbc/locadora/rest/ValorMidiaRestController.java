package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.service.ValorMidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author douglas.marques
 */
@RestController
@RequestMapping("/api/valormidia")
public class ValorMidiaRestController extends AbstractRestController<ValorMidia, Long, ValorMidiaService> {

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected ValorMidiaService getService() {
        return valorMidiaService;
    }
}
