package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas.marques
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {
    
    public int countByTipo(MidiaType tipo);
    
    public int countByTipoAndIdFilmeId(MidiaType tipo, Long id);
    
    public List<Midia> findByIdFilme (Filme idFilme);
    
    public List<Midia> findByIdFilmeId (Long idFilme);
    
    public List<Midia> findByIdFilmeIdAndIdAluguelIsNull (Long idFilme);
}
