public class FormaGeometricaFactory {
    public static Retangulo cria(int tipo, int x, int y) {
        switch(tipo) {
            case 1:
                return new Retangulo(x, y);
            case 2:
                return new Quadrado(x);
            default:
                return null;
        }
    }
    
    public static FormaGeometrica criar(int tipo, int x, int y) {
        switch(tipo) {
            case 1:
                return new RetanguloImpl(x, y);
            case 2:
                return new QuadradoImpl(x);
            default:
                return null;
        }
    }
}