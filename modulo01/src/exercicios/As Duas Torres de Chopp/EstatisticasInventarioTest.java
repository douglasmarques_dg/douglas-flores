import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class EstatisticasInventarioTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void calcularMediaTest() {
        Inventario itens = new Inventario(10);
        Item[] itensArr = {
            new Item("Machado", 1),
            new Item("Lança", 3),
            new Item("Arco", 3),
            new Item("Flecha", 6),
            new Item("Poção de cura", 7),
            new Item("Poção de Magia", 8)
        };
        for(Item i : itensArr) itens.adicionarItem(i);
        EstatisticasInventario estInv = new EstatisticasInventario(itens);
        assertEquals(4, estInv.calcularMedia());
    }
    
    @Test
    public void calcularMedianaImparTest() {
        Inventario itens = new Inventario(10);
        Item[] itensArr = {
            new Item("Machado", 1),
            new Item("Lança", 3),
            new Item("Arco", 2),
            new Item("Flecha", 9),
            new Item("Poção de cura", 6),
            new Item("Poção de Magia", 2),
            new Item("Espada", 9)
        };
        for(Item i : itensArr) itens.adicionarItem(i);
        EstatisticasInventario estInv = new EstatisticasInventario(itens);        
        assertEquals(3, estInv.calcularMediana());
    }
        
    @Test
    public void calcularMedianaParTest() {
        Inventario itens = new Inventario(10);
        Item[] itensArr = {
            new Item("Machado", 1),
            new Item("Lança", 3),
            new Item("Arco", 2),
            new Item("Flecha", 9),
            new Item("Poção de cura", 6),
            new Item("Poção de Magia", 2)
        };
        for(Item i : itensArr) itens.adicionarItem(i);
        EstatisticasInventario estInv = new EstatisticasInventario(itens);
        assertEquals(2, estInv.calcularMediana());
    }
    
    @Test
    public void qtdItensAcimaDaMediaTest() {
        Inventario itens = new Inventario(10);
        Item[] itensArr = {
            new Item("Machado", 1),
            new Item("Lança", 3),
            new Item("Arco", 2),
            new Item("Flecha", 9),
            new Item("Poção de cura", 6),
            new Item("Poção de Magia", 2)
        };
        for(Item i : itensArr) itens.adicionarItem(i);
        EstatisticasInventario estInv = new EstatisticasInventario(itens);
        assertEquals(2, estInv.qtdItensAcimaDaMedia());
    }
    
    @Test
    public void ordenarTest() {
        Inventario itens = new Inventario(10);
        Item[] itensArr = {
            new Item("Machado", 1),
            new Item("Lança", 3),
            new Item("Arco", 2),
            new Item("Flecha", 9),
            new Item("Poção de cura", 6),
            new Item("Poção de Magia", 2),
            new Item("Espada", 9)
        };
        for(Item i : itensArr) itens.adicionarItem(i);
        EstatisticasInventario estInv = new EstatisticasInventario(itens);
                
        ArrayList<Item> itensOrdenadosNaturalmente = new ArrayList<>();
        itensOrdenadosNaturalmente.add(new Item("Machado", 1));
        itensOrdenadosNaturalmente.add(new Item("Arco", 2));
        itensOrdenadosNaturalmente.add(new Item("Poção de Magia", 2));
        itensOrdenadosNaturalmente.add(new Item("Lança", 3));
        itensOrdenadosNaturalmente.add(new Item("Poção de cura", 6));
        itensOrdenadosNaturalmente.add(new Item("Flecha", 9));
        itensOrdenadosNaturalmente.add(new Item("Espada", 9));
        
        ArrayList<Item> itensOrdenadosComOMetodo = estInv.bubbleSort();
        for(int i = 0; i < itens.tamanho(); i++)
            assertEquals(itensOrdenadosNaturalmente.get(i).getQuantidade(), itensOrdenadosComOMetodo.get(i).getQuantidade());
    }
}