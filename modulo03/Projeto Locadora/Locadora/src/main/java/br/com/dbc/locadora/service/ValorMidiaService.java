package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas.marques
 */
@Service
public class ValorMidiaService extends AbstractCRUDService<ValorMidia, Long> {

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Override
    protected JpaRepository<ValorMidia, Long> getRepository() {
        return valorMidiaRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void savePreco(Midia midia, Double valor) {
        valorMidiaRepository.save(ValorMidia.builder()
                .valor(valor)
                .inicioVigencia(LocalDateTime.now())
                .finalVigencia(null)
                .idMidia(midia)
                .build());
    }

    public double findByMidia(Midia midia) {
        return valorMidiaRepository.findByidMidiaAndFinalVigenciaIsNull(midia).orElseGet(null).getValor();
    }
    
    public ValorMidia findByMidiaType(MidiaType tipo) {
        return valorMidiaRepository.findByidMidiaTipoAndFinalVigenciaIsNull(tipo).orElseGet(null);
    }
    
    public List<ValorMidia> findByidMidiaId(Long idFilme) {
        return valorMidiaRepository.findByidMidiaId(idFilme);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateByIdMidia(Midia idMidia, Double valor) {
        Optional<ValorMidia> optionalMidia = valorMidiaRepository.findByidMidiaAndFinalVigenciaIsNull(idMidia);
        if (!optionalMidia.isPresent()) {
            return;
        }        
        ValorMidia vMidia = optionalMidia.get();        
        if (vMidia.getValor() == valor) {
            return;
        }
        vMidia.setFinalVigencia(LocalDateTime.now());
        valorMidiaRepository.save(vMidia);
        valorMidiaRepository.save(ValorMidia.builder()
                .id(null)
                .valor(valor)
                .inicioVigencia(LocalDateTime.now())
                .finalVigencia(null)
                .idMidia(idMidia)
                .build()
        );
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteByIdMidiaId(Midia idMidia) {
        valorMidiaRepository.deleteByIdMidiaId(idMidia.getId());
    }
}
