package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.UserDTO;
import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas.marques
 */
@Service
public class UserService extends AbstractCRUDService<User, Long> {

    @Autowired
    private UserRepository userRepository;

    @Override
    protected JpaRepository<User, Long> getRepository() {
        return userRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public User updatePassword(UserDTO userDTO) {
        User user = userRepository.findByUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        super.save(user);
        return user;
    }
}
