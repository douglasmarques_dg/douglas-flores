package com.petshop.DAO;

import com.petshop.entity.Cliente;

/**
 *
 * @author douglas.marques
 */
public class ClienteDAO extends GenericDAO<Cliente, Long> {

    private static final ClienteDAO instance;
    
    static {
        instance = new ClienteDAO();
    }
    
    public static ClienteDAO getInstance(){
        return instance;
    }

    @Override
    protected Class<Cliente> getEntityClass() {
        return Cliente.class;
    }

    @Override
    protected String getIdProperty() {
        return "id";
    }
}
