
/**
 * Escreva a descrição da classe Numero aqui.
 * 
 * @author Douglas Marques 
 * @version 09.04.18
 */
public class Numero {
    private int num;
    
    public Numero(int num) {
        this.num = num;
    }
    
    public boolean impar() {
        return !(this.num % 2 == 0);
    }
    
    public int getNumero() {
        return this.num;
    }
    
    // Exercicio 3
    public boolean verificarSomaDivisivel(int num) {
        if(num == 0) return true;
        int sum = 0;
        String temp = Integer.toString(num);
        int[] newNum = new int[temp.length()];
        for(int i = 0; i < temp.length(); i++) {
            newNum[i] = temp.charAt(i) - '0';
            sum += newNum[i];
        }             
        return sum == 0 || sum % this.num == 0;
    }
            
}
