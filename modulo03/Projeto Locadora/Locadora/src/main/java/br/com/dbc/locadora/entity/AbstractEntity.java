package br.com.dbc.locadora.entity;

/**
 *
 * @author douglas.marques
 * @param <ID>
 */
public abstract class AbstractEntity<ID> {

    public abstract ID getId();
}
