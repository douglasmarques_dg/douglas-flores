package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas.marques
 */
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    
    public Page<Cliente> findByNomeContainingIgnoreCase(Pageable pageable, String nome);
    
    public Page<Cliente> findByTelefone(Pageable pageable, String telefone);
    
    public Page<Cliente> findByBairroIgnoreCase(Pageable pageable,String endereco);
}
