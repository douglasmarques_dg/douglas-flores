package br.com.dbc.locadora.dto;

import java.io.Serializable;
import java.util.ArrayList;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author douglas.marques
 */
@Data
@Builder
public class AluguelDTO implements Serializable {
    
    private Long idCliente; 
    
    private ArrayList<Long> midias;
}
