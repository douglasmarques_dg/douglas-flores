package com.petshop.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author douglas.marques
 */
@Entity
@Table(name = "ANIMAL")
@NamedQueries({
    @NamedQuery(name = "Animal.findAll", query = "SELECT a FROM Animal a")
    , @NamedQuery(name = "Animal.findById", query = "SELECT a FROM Animal a WHERE a.id = :id")
    , @NamedQuery(name = "Animal.findByNomeanimal", query = "SELECT a FROM Animal a WHERE a.nomeanimal = :nomeanimal")})
@SequenceGenerator(name = "ANIMAL_SEQUENCE", sequenceName = "ANIMAL_SEQUENCE", allocationSize = 1)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Animal extends Generic<Long> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "IDANIMAL", nullable = false)
    @GeneratedValue(generator = "ANIMAL_SEQUENCE", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "NOMEANIMAL", nullable = false)
    private String nomeanimal;
    @ToString.Exclude
    @ManyToMany(mappedBy = "animalList")
    private List<Cliente> clienteList;
}
