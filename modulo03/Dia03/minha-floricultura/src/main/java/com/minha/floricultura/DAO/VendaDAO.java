package com.minha.floricultura.DAO;

import com.minha.floricultura.entity.Venda;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author douglas.marques
 */
@Stateless
public class VendaDAO extends AbstractDAO<Venda> {

    @PersistenceContext(unitName = "minha-floricultura-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VendaDAO() {
        super(Venda.class);
    }
}
