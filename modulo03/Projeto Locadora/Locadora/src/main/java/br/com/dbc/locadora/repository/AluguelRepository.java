package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Aluguel;
import java.time.LocalDateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas.marques
 */
public interface AluguelRepository extends JpaRepository<Aluguel, Long> {

    public Page<Aluguel> findByPrevisaoBetween(Pageable pageable, LocalDateTime previsao, LocalDateTime previsaoFim);
}
