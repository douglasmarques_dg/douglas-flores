public class Programa {
    public static void main(String[] args) {
        int entrada = 2, x = 5, y = 10;
        Retangulo formaGeometrica = FormaGeometricaFactory.cria(entrada, x, y);
        System.out.println(formaGeometrica.calcularArea());
    }
}