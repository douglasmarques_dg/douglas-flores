public class Elfo extends Personagem {
    protected int experiencia;
    protected static int quantElfos = 0;
    
    public Elfo(String nome) {
        super(nome, 100.0, Status.VIVO);
        super.inv.adicionarItem(new Item("Arco", 1));
        super.inv.adicionarItem(new Item("Flecha", 7));
        Elfo.quantElfos++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.quantElfos--;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public Item getFlecha() {
        return this.inv.buscarItem("Flecha");
    }
        
    public void atirarFlecha(Dwarf dwarf) {
        if(this.getFlecha().getQuantidade() == 0) return;
        this.getFlecha().setQuantidade(this.getFlecha().getQuantidade() - 1);        
        super.receberDano();
        if(dwarf.receberDano()) this.experiencia+=this.QTD_XP;
    }
    
    public static int quantElfos() {
        return quantElfos;
    }
} 