package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.SexoType;
import java.util.List;

/**
 *
 * @author douglas.flores
 */
public class Main {

    private static final AnimalDAOJPA instanceAnimal = new AnimalDAOJPA();
    private static final ClienteDAOJPA instanceCliente = new ClienteDAOJPA();

    public static void insereDezClientes() {
        for (int i = 0; i <= 9; i++) {
            Cliente cliente = new Cliente(null, String.format("Cliente %d", i), SexoType.F, String.format("Profissao %d", i));
            instanceCliente.persistir(cliente);
            retornaDezAnimais(cliente);
            instanceCliente.merge(cliente);
        }
    }

    public static void retornaDezAnimais(Cliente c) {
        long min = 1L;
        long max = 100L;
        long precoAleatorio = min + (long) (Math.random() * (max - min));
        for (int i = 0; i <= 9; i++) {            
            Animal animal = new Animal(null, String.format("Animal %d", i), SexoType.F, c, precoAleatorio);
            instanceAnimal.persistir(animal);
            c.addAnimalList(animal);
        }
    }
    
    public static void imprimeDonoEQuantidade() {
        List<Cliente> clientes = instanceCliente.findAll();
        System.out.println("Clientes | Quantidade de animais");
        clientes.forEach(c -> System.out.println(String.format("Cliente: %s - Animais: %d", c.getNome(), c.getAnimalList().size())));
    }

    public static void main(String... args) {
        System.out.println("Banco vazio");
        System.out.println(instanceAnimal.findAll());
        System.out.println(instanceCliente.findAll());

        insereDezClientes();

        System.out.println("Banco com 10 clientes com 10 animais cada");
        System.out.println(instanceAnimal.findAll());
        System.out.println(instanceCliente.findAll());

        imprimeDonoEQuantidade();
    }
}
