package br.com.dbc.locadora.entity;

/**
 *
 * @author douglas.marques
 */
public enum MidiaType {
    DVD, VHS, BLUE_RAY
}
