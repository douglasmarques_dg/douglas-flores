package com.exercicio.DAO;

import com.exercicio.model.Produto;
import com.exercicio.util.PersistenceUtil;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author douglas.marques
 */
public class ProdutoDAOTest {

    public ProdutoDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        PersistenceUtil.beginTransaction();
        PersistenceUtil.getEm().createNativeQuery("delete from produto").executeUpdate();
        PersistenceUtil.getEm().getTransaction().commit();
    }

    /**
     * Test of getById method, of class ProdutoDAO.
     */
    @Test
    public void testGetById() {
        System.out.println("getById");
        long id = 0L;
        ProdutoDAO instance = new ProdutoDAO();
        Produto expResult = null;
        Produto result = instance.getById(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of findAll method, of class ProdutoDAO.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        ProdutoDAO instance = new ProdutoDAO();
        List<Produto> result = instance.findAll();
        assertEquals(0, result.size());
    }

    /**
     * Test of persistir method, of class ProdutoDAO.
     */
    @Test
    public void testPersistir() {
        System.out.println("persistir");

        ProdutoDAO instance = new ProdutoDAO();

        List<Produto> result = instance.findAll();
        assertEquals(0, result.size());

        Produto produto = new Produto(null, "Computador");
        instance.persistir(produto);

        result = instance.findAll();
        assertEquals(1, result.size());
    }

    /**
     * Test of merge method, of class ProdutoDAO.
     */
    @Test
    public void testMerge() {
        System.out.println("merge");

        ProdutoDAO instance = new ProdutoDAO();

        List<Produto> result = instance.findAll();
        assertEquals(0, result.size());

        Produto produto = new Produto(null, "Notebook");
        instance.merge(produto);

        result = instance.findAll();
        assertEquals(1, result.size());

        produto.setDescricaoproduto("Laptop");
        instance.merge(produto);

        result = instance.findAll();
        assertEquals(1, result.size());
    }

    /**
     * Test of remover method, of class ProdutoDAO.
     */
    @Test
    public void testRemover() {
        System.out.println("remover");

        ProdutoDAO instance = new ProdutoDAO();

        List<Produto> result = instance.findAll();
        assertEquals(0, result.size());

        Produto produto = new Produto(null, "SSD");
        instance.persistir(produto);

        result = instance.findAll();
        assertEquals(1, result.size());

        instance.remover(produto);

        result = instance.findAll();
        assertEquals(0, result.size());
    }

    /**
     * Test of removeById method, of class ProdutoDAO.
     */
    @Test
    public void testRemoveById() {
        System.out.println("removeById");

        ProdutoDAO instance = new ProdutoDAO();

        List<Produto> result = instance.findAll();
        assertEquals(0, result.size());

        Produto produto = new Produto(null, "CD");
        instance.persistir(produto);

        result = instance.findAll();
        assertEquals(1, result.size());

        instance.removeById(produto.getIdproduto());

        result = instance.findAll();
        assertEquals(0, result.size());
    }
}
