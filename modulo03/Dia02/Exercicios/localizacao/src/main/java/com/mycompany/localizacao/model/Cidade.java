package com.mycompany.localizacao.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author douglas.marques
 */
@Entity
@Table(name = "CIDADE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cidade.findAll", query = "SELECT c FROM Cidade c")
    , @NamedQuery(name = "Cidade.findByIdcidade", query = "SELECT c FROM Cidade c WHERE c.idcidade = :idcidade")
    , @NamedQuery(name = "Cidade.findByNamecidade", query = "SELECT c FROM Cidade c WHERE c.namecidade = :namecidade")
    , @NamedQuery(name = "Cidade.findBySiglacidade", query = "SELECT c FROM Cidade c WHERE c.siglacidade = :siglacidade")})
public class Cidade implements Serializable {

    private static final Long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDCIDADE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CIDADE_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ")
    private Long idcidade;
    @Basic(optional = false)
    @Column(name = "NAMECIDADE")
    private String namecidade;
    @Column(name = "SIGLACIDADE")
    private String siglacidade;
    @JoinColumn(name = "IDESTADO", referencedColumnName = "IDESTADO")
    @ManyToOne(optional = false)
    private Estado idestado;

    public Cidade() {
    }

    public Cidade(Long idcidade) {
        this.idcidade = idcidade;
    }

    public Cidade(Long idcidade, String namecidade) {
        this.idcidade = idcidade;
        this.namecidade = namecidade;
    }

    public Long getIdcidade() {
        return idcidade;
    }

    public void setIdcidade(Long idcidade) {
        this.idcidade = idcidade;
    }

    public String getNamecidade() {
        return namecidade;
    }

    public void setNamecidade(String namecidade) {
        this.namecidade = namecidade;
    }

    public String getSiglacidade() {
        return siglacidade;
    }

    public void setSiglacidade(String siglacidade) {
        this.siglacidade = siglacidade;
    }

    public Estado getIdestado() {
        return idestado;
    }

    public void setIdestado(Estado idestado) {
        this.idestado = idestado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcidade != null ? idcidade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cidade)) {
            return false;
        }
        Cidade other = (Cidade) object;
        if ((this.idcidade == null && other.idcidade != null) || (this.idcidade != null && !this.idcidade.equals(other.idcidade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.localizacao.model.Cidade[ idcidade=" + idcidade + " ]";
    }
    
}
