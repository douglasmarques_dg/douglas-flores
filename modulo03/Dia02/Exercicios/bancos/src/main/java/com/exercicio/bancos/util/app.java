package com.exercicio.bancos.util;

import com.exercicio.bancos.DAO.BancoDAO;
import com.exercicio.bancos.model.Banco;

/**
 *
 * @author douglas.marques
 */
// Só pra avisar que fiz uma cagada e coloquei nomeBanco e nomeCliente como Long, mas não tô afim de corrigir agora.
public class app {
    private static final BancoDAO bancoDAO = new BancoDAO();
    
    public static void main(String... args) {
        Banco teste = new Banco(null, 1L);
        bancoDAO.persistir(teste);
    }
}
