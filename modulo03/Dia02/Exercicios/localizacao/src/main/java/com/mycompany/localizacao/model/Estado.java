package com.mycompany.localizacao.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author douglas.marques
 */
@Entity
@Table(name = "ESTADO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estado.findAll", query = "SELECT e FROM Estado e")
    , @NamedQuery(name = "Estado.findByIdestado", query = "SELECT e FROM Estado e WHERE e.idestado = :idestado")
    , @NamedQuery(name = "Estado.findByNameestado", query = "SELECT e FROM Estado e WHERE e.nameestado = :nameestado")
    , @NamedQuery(name = "Estado.findBySiglaestado", query = "SELECT e FROM Estado e WHERE e.siglaestado = :siglaestado")})
public class Estado implements Serializable {

    private static final Long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDESTADO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESTADO_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ")
    private Long idestado;
    @Basic(optional = false)
    @Column(name = "NAMEESTADO")
    private String nameestado;
    @Column(name = "SIGLAESTADO")
    private String siglaestado;
    @JoinColumn(name = "IDPAIS", referencedColumnName = "IDPAIS")
    @ManyToOne(optional = false)
    private Pais idpais;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idestado")
    private List<Cidade> cidadeList;

    public Estado() {
    }

    public Estado(Long idestado) {
        this.idestado = idestado;
    }

    public Estado(Long idestado, String nameestado) {
        this.idestado = idestado;
        this.nameestado = nameestado;
    }

    public Long getIdestado() {
        return idestado;
    }

    public void setIdestado(Long idestado) {
        this.idestado = idestado;
    }

    public String getNameestado() {
        return nameestado;
    }

    public void setNameestado(String nameestado) {
        this.nameestado = nameestado;
    }

    public String getSiglaestado() {
        return siglaestado;
    }

    public void setSiglaestado(String siglaestado) {
        this.siglaestado = siglaestado;
    }

    public Pais getIdpais() {
        return idpais;
    }

    public void setIdpais(Pais idpais) {
        this.idpais = idpais;
    }

    @XmlTransient
    public List<Cidade> getCidadeList() {
        return cidadeList;
    }

    public void setCidadeList(List<Cidade> cidadeList) {
        this.cidadeList = cidadeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idestado != null ? idestado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estado)) {
            return false;
        }
        Estado other = (Estado) object;
        if ((this.idestado == null && other.idestado != null) || (this.idestado != null && !this.idestado.equals(other.idestado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.localizacao.model.Estado[ idestado=" + idestado + " ]";
    }
    
}
