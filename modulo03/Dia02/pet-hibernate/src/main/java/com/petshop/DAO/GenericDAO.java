package com.petshop.DAO;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author douglas.marques
 */
public abstract class GenericDAO<ENTITY, ID> {

    private static final Logger LOG = Logger.getLogger(GenericDAO.class.getName());

    protected abstract Class<ENTITY> getEntityClass();

    protected abstract String getIdProperty();

    public GenericDAO() {
    }

    public List<ENTITY> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(getEntityClass()).list();
    }

    public ENTITY findById(ID id) {
        return getEntityClass().cast(HibernateUtil.getSessionFactory()
                .openSession()
                .createCriteria(getEntityClass())
                .add(Restrictions.eq(getIdProperty(), id))
                .uniqueResult());
    }

    public void saveOrUpdate(ENTITY entity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            session.saveOrUpdate(entity);
            t.commit();
        } catch (Exception e) {
            t.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        } finally {
            session.close();
        }
    }

    public void delete(ENTITY entity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            session.delete(entity);
            t.commit();
        } catch (Exception e) {
            t.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        } finally {
            session.close();
        }
    }
}
