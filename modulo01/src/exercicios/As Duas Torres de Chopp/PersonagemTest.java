import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class PersonagemTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void ganharItemTest() {
        Personagem npc = new Elfo("NPC");
        npc.ganharItem(new Item("Espada", 1));
        assertEquals(new Item("Espada", 1), npc.inv.obter(2));
        assertEquals(3, npc.inv.tamanho());
        assertEquals(new Item("Espada", 1), npc.inv.buscarItem("Espada"));
        npc.ganharItem(new Item("Escudo", 1));
        assertEquals(new Item("Escudo", 1), npc.inv.obter(3));
        assertEquals(4, npc.inv.tamanho());
        assertEquals(new Item("Escudo", 1), npc.inv.buscarItem("Escudo"));
    }
    
    @Test
    public void ganharItemDuplicadoTest() {
        Personagem npc = new Dwarf("NPC");
        npc.ganharItem(new Item("Espada", 1));
        assertEquals(new Item("Espada", 1), npc.inv.obter(0));
        assertEquals(1, npc.inv.tamanho());
        assertEquals(new Item("Espada", 1), npc.inv.buscarItem("Espada"));
        npc.ganharItem(new Item("Espada", 1));
        assertEquals(2, npc.inv.obter(0).getQuantidade());
        assertEquals(new Item("Espada", 2), npc.inv.obter(0));
        assertEquals(1, npc.inv.tamanho());
    }
    
    @Test
    public void perderItemTest() {
        Personagem npc = new ElfoVerde("NPC");
        npc.ganharItem(new Item("Espada", 1));
        npc.ganharItem(new Item("Escudo", 1));
        npc.perdeItem(new Item("Espada", 1));
        assertEquals(2, npc.inv.tamanho());
        npc.perdeItem(new Item("Escudo", 1));
        assertEquals(2, npc.inv.tamanho());
    }
    
    @Test
    public void getInventarioTest() {
        Personagem npc = new ElfoNoturno("NPC");
        npc.ganharItem(new Item("Espada", 1));
        npc.ganharItem(new Item("Escudo", 1));
        ArrayList<Item> inv = new ArrayList(5);
        inv.add(new Item("Arco", 1));
        inv.add(new Item("Flecha", 7));
        inv.add(new Item("Espada", 1));
        inv.add(new Item("Escudo", 1));
        assertArrayEquals(inv.toArray(), npc.getInventario().getItems().toArray());
    }
    
    @Test
    public void getInventarioNullTest() {
        Personagem npc = new Dwarf("NPC");
        assertEquals(0, npc.inv.tamanho());
    }
}