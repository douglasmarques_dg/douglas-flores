package com.minha.floricultura.WS;

import com.minha.floricultura.DAO.ProdutoDAO;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import com.minha.floricultura.entity.Produto;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author douglas.marques
 */
@Stateless
@WebService(serviceName = "ProdutoWS")
public class ProdutoWS extends AbstractCRUDWS<ProdutoDAO, Produto> {

    @EJB
    private ProdutoDAO produtoDAO;

    @Override
    @WebMethod(exclude = true)
    public ProdutoDAO getDAO() {
        return produtoDAO;
    }

    @WebMethod(operationName = "findDescricao")
    public List<Produto> findDescricao(@WebParam(name = "descricao") String descricao) {
        return produtoDAO.findDescricao(descricao);
    }
}
