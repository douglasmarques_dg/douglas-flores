package br.com.dbc.locadora.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas.marques
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Aluguel extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "RETIRADA")
    private LocalDateTime retirada;

    @NotNull
    @Column(name = "PREVISAO")
    private LocalDateTime previsao;

    @Column(name = "DEVOLUCAO")
    private LocalDateTime devolucao;

    @Column(name = "MULTA")
    private Double multa;

    @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Cliente idCliente;
}
