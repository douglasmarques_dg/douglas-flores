import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class InventarioTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void obterItemTest() {
        Inventario itens = new Inventario(10);
        Item esperado = new Item("Espada", 5);
        itens.adicionarItem(esperado);
        assertEquals(esperado, itens.obter(0));
    }
    
    @Test
    public void removerItemTest() {
        Inventario itens = new Inventario(10);
        Item esperado = new Item("Armadura", 5);
        itens.adicionarItem(esperado);  
        assertEquals(0, itens.getItems().indexOf(esperado));
        assertEquals(esperado, itens.remover(0));
        //assertEquals(null, itens.obter(0));
    }
    
    @Test
    public void maiorQuantidadeTest() {
        Inventario itens = new Inventario(10);
        Item[] itensArr = new Item[]{
            new Item("Machado", 1),
            new Item("Lança", 3),
            new Item("Arco", 2),
            new Item("Flecha", 9),
        };
        for(int i = 0; i < itensArr.length; i++) 
            itens.adicionarItem(itensArr[i]); 
        assertEquals(9, itens.maiorQuantidade().getQuantidade());        
    }
    
    @Test
    public void buscarItemTest() {
        Inventario itens = new Inventario(10);
        Item esperado = new Item("Escudo", 5);
        itens.adicionarItem(esperado);  
        assertEquals(esperado, itens.buscarItem(esperado.getDescricao()));
    }
    
    @Test
    public void buscarItemFailTest() {
        Inventario itens = new Inventario(10);
        assertEquals(null, itens.buscarItem("Hidromel"));
    }
    
    @Test
    public void inverterTest() {
        Inventario itens = new Inventario(10);
        Item[] itensArr = {
            new Item("Machado", 1),
            new Item("Lança", 3),
            new Item("Arco", 2),
            new Item("Flecha", 9),
            new Item("Poção de cura", 6),
            new Item("Poção de Magia", 2)
        };
        for(Item i : itensArr) itens.adicionarItem(i);
        List lista = Arrays.asList(itensArr);
        Collections.reverse(lista);
        assertEquals(lista, itens.inverter());
    }
    
    @Test
    public void ordenarItemsTest() {
        Inventario itens = new Inventario(10);
        Item[] itensArr = {
            new Item("Machado", 1),
            new Item("Lança", 3),
            new Item("Arco", 2),
            new Item("Flecha", 9),
            new Item("Poção de cura", 6),
            new Item("Poção de Magia", 2),
            new Item("Espada", 9)
        };
        for(Item i : itensArr) itens.adicionarItem(i);
                
        ArrayList<Item> itensOrdenadosNaturalmente = new ArrayList<>();
        itensOrdenadosNaturalmente.add(new Item("Machado", 1));
        itensOrdenadosNaturalmente.add(new Item("Arco", 2));
        itensOrdenadosNaturalmente.add(new Item("Poção de Magia", 2));
        itensOrdenadosNaturalmente.add(new Item("Lança", 3));
        itensOrdenadosNaturalmente.add(new Item("Poção de cura", 6));
        itensOrdenadosNaturalmente.add(new Item("Flecha", 9));
        itensOrdenadosNaturalmente.add(new Item("Espada", 9));
        
        itens.ordenarItens();
        for(int i = 0; i < itens.tamanho(); i++)
            assertEquals(itensOrdenadosNaturalmente.get(i).getQuantidade(), itens.obter(i).getQuantidade());
    }
}