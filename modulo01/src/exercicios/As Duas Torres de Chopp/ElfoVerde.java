public class ElfoVerde extends Elfo {    
    public ElfoVerde(String nome) {
        super(nome);
        QTD_XP *= 2;
        QTD_DANO = 10;
    }
    
    public void ganharItem(Item it) {
        if(it.getDescricao().equals("Espada de aço valiriano") || it.getDescricao().equals("Arco de Vidro") || it.getDescricao().equals("Flecha de Vidro")) super.ganharItem(it);;
    }
}