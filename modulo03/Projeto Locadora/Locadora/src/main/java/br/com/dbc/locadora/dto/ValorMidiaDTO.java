package br.com.dbc.locadora.dto;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author douglas.marques
 */
@Data
public class ValorMidiaDTO implements Serializable {

    private Double valor;
}
