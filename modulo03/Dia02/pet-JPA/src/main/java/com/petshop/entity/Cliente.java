package com.petshop.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author douglas.marques
 */
@Entity
@Table(name = "CLIENTE")
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
    , @NamedQuery(name = "Cliente.findById", query = "SELECT c FROM Cliente c WHERE c.id = :id")
    , @NamedQuery(name = "Cliente.findByNomecliente", query = "SELECT c FROM Cliente c WHERE c.nomecliente = :nomecliente")})
@SequenceGenerator(name = "CLIENTE_SEQUENCE", sequenceName = "CLIENTE_SEQUENCE", allocationSize = 1)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cliente extends Generic<Long> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "IDCLIENTE", nullable = false)
    @GeneratedValue(generator = "CLIENTE_SEQUENCE", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "NOMECLIENTE", nullable = false)
    private String nomecliente;
    @JoinTable(name = "CLIENTE_ANIMAL", joinColumns = {
        @JoinColumn(name = "IDCLIENTE", referencedColumnName = "IDCLIENTE")}, inverseJoinColumns = {
        @JoinColumn(name = "IDANIMAL", referencedColumnName = "IDANIMAL")})
    @ToString.Exclude
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Animal> animalList;
}
