import java.util.*;

public class ExercitoElfos {
    private static final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
    ));
    private ArrayList<Elfo> exercito;
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    
    public ExercitoElfos(){
        exercito = new ArrayList<>();
    }
    
    protected ArrayList<Elfo> buscar(Status s) {
        // if(tamanho() == 0) return null;
        // ArrayList<Elfo> exerc = new ArrayList<>();
        // for(Elfo p : exercito)
            // if(p.getStatus() == s) exerc.add(p);
        // return exerc;
        return porStatus.get(s);
    }
    
    protected int tamanho() {        
        return this.exercito.size();
    }
    
    protected ArrayList<Elfo> getElfos() {
        return this.exercito;
    }
    
    protected ArrayList<Elfo> vivos() {
        return buscar(Status.VIVO);
    }
    
    protected ArrayList<Elfo> mortos() {
        return buscar(Status.MORTO);
    }
    
    public void alistar(Elfo elf) {
        if(TIPOS_PERMITIDOS.contains(elf.getClass())) {
            exercito.add(elf);
            ArrayList<Elfo> elfosDoStatus = this.porStatus.get(elf.getStatus());
            if(elfosDoStatus == null) {
                elfosDoStatus = new ArrayList<>();
                this.porStatus.put(elf.getStatus(), elfosDoStatus);
            }
            elfosDoStatus.add(elf);
        }
    }
    
}