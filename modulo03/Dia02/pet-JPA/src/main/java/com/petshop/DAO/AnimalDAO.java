package com.petshop.DAO;

import com.petshop.entity.Animal;

/**
 *
 * @author douglas.marques
 */
public class AnimalDAO extends GenericDAO<Animal, Long> {

    private static final AnimalDAO instance;
    
    static {
        instance = new AnimalDAO();
    }
    
    public static AnimalDAO getInstance(){
        return instance;
    }

    @Override
    protected Class<Animal> getEntityClass() {
        return Animal.class;
    }

    @Override
    protected String getIdProperty() {
        return "id";
    }
}
