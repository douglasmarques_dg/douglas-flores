import java.util.*;

public class NoturnosUltimo implements Estrategia {    
    public List<Elfo> getOrdemDeAtaque(List<Elfo> atacantes) {
        List<Elfo> formacao = new ArrayList<>();
        if(atacantes == null || atacantes.size() == 0) return formacao;        
        for(Elfo elfo : atacantes)
            if(elfo.getStatus() == Status.VIVO)
                if(elfo instanceof ElfoVerde)
                    formacao.add(0, elfo);
                else 
                    formacao.add(elfo);
        if(formacao.size() == 0) return null;
        return formacao;
    }
}