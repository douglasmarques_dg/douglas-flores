package br.com.dbc.minhaseguradora.service;

import br.com.dbc.minhaseguradora.entity.Apolice;
import br.com.dbc.minhaseguradora.repository.ApoliceRepository;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas.marques
 */
@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class ApoliceService {

    @Autowired
    private final ApoliceRepository aR;

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Apolice save(@NotNull @Valid Apolice apolice) {
        return aR.save(apolice);
    }

    public Page<Apolice> findAll(Pageable pageable) {
        return aR.findAll(pageable);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete(Long id) {
        aR.deleteById(id);
    }

    public Optional<Apolice> findById(Long id) {
        return aR.findById(id);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Apolice update(Long id, Apolice input) {
        input.setId(id);
        return aR.save(input);
    }
}
