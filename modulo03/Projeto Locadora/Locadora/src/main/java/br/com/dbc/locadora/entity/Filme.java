package br.com.dbc.locadora.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author douglas.marques
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Filme extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "TITULO")
    private String titulo;

    @NotNull
    @Column(name = "LANCAMENTO")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate lancamento;

    @NotNull
    @Column(name = "CATEGORIA")
    @Enumerated(EnumType.STRING)
    private Categoria categoria;
}
