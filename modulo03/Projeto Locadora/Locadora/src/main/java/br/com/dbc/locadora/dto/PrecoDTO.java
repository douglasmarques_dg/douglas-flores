package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.MidiaType;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author douglas.marques
 */
@Data
@Builder
public class PrecoDTO {
    
    private Long id;
    
    private MidiaType tipo;
    
    private double valor;
    
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDateTime inicioVigencia;
    
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDateTime finalVigencia;
}
