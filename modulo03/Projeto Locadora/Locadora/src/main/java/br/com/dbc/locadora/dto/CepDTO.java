package br.com.dbc.locadora.dto;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author douglas.marques
 */
@Data
@Builder
public class CepDTO {

    private String rua;
    private String bairro;
    private String cidade;
    private String estado;
}
