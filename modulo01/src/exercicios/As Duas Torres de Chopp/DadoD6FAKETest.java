import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6FAKETest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void sortearUmTest() {
        DadoD6FAKE D6 = new DadoD6FAKE();
        D6.setValorFalso(1);
        assertEquals(1, D6.sortear());
    }
    
    @Test
    public void sortearDoisTest() {
        DadoD6FAKE D6 = new DadoD6FAKE();
        D6.setValorFalso(2);
        assertEquals(2, D6.sortear());
    }
    
    @Test
    public void sortearTresTest() {
        DadoD6FAKE D6 = new DadoD6FAKE();
        D6.setValorFalso(3);
        assertEquals(3, D6.sortear());
    }
    
    @Test
    public void sortearQuatroTest() {
        DadoD6FAKE D6 = new DadoD6FAKE();
        D6.setValorFalso(4);
        assertEquals(4, D6.sortear());
    }
    
    @Test
    public void sortearCincoTest() {
        DadoD6FAKE D6 = new DadoD6FAKE();
        D6.setValorFalso(5);
        assertEquals(5, D6.sortear());
    }
    
    @Test
    public void sortearSeisTest() {
        DadoD6FAKE D6 = new DadoD6FAKE();
        D6.setValorFalso(6);
        assertEquals(6, D6.sortear());
    }
    
    @Test
    public void sortearSeteTest() {
        DadoD6FAKE D6 = new DadoD6FAKE();
        D6.setValorFalso(7);
        assertNotEquals(7, D6.sortear());
    }
}