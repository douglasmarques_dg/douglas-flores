describe("naoBissexto", function() {
  beforeEach(function() {
    chai.should();
  });

  it("deve calcular se 2016 é ano bissexto", function() {
    const ano = 2016;
    const resultado = naoBissexto(ano);
    resultado.should.equal(false);
  });

  it("deve calcular se 2017 é ano bissexto", function() {
    const ano = 2017;
    const resultado = naoBissexto(ano);
    resultado.should.equal(true);
  });
});
