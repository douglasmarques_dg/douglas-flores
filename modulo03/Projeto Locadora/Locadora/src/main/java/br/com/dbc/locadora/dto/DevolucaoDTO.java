package br.com.dbc.locadora.dto;

import java.util.ArrayList;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author douglas.marques
 */
@Data
@Builder
public class DevolucaoDTO {
    
    private ArrayList<Long> midias;
}
