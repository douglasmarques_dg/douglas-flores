package com.petshop.entity.service;

import com.petshop.DAO.GenericDAO;
import com.petshop.entity.Generic;
import java.util.List;
import javassist.NotFoundException;

public abstract class GenericService<ENTITY extends Generic<ID>, 
                                           ID,
                                           DAO extends GenericDAO<ENTITY,ID >> {
    
    protected abstract DAO getDao();
    
    public List<ENTITY> findAll(){
        return getDao().findAll();
    }
    
    public ENTITY findOne(ID id){
        return getDao().findById(id);
    }
    
    public void create(ENTITY entity){
        if(entity.getId() != null){
            throw new IllegalArgumentException("Criação de Cliente não pode ter ID");
        }
        getDao().saveOrUpdate(entity);
    }
    
    public void update(ENTITY entity){
        if(entity.getId() == null){
            throw new IllegalArgumentException("Update de Cliente não pode ter ID NULL");
        }
        getDao().saveOrUpdate(entity);
    }
    
    public void delete(ID id) throws NotFoundException{
        if(id == null){
            throw new IllegalArgumentException("Deletar um Cliente não pode ter ID NULL");
        }
        ENTITY entity = findOne(id);
        if(entity == null){
            throw new NotFoundException("");
        }
        getDao().delete(entity);
    }
}