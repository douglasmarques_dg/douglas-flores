const campo = document.querySelector("#num");
const img = document.querySelector("#img");
const tipos = document.querySelector("#info");
const sorte = document.querySelector("#sorte");
let id = 0;

function checarNumero() {
  let num = campo.value;
  if (num < 1 || num >= 802) alert("ID inválido!");
  else {
    if (economizar(num)) buscaPokemon(num);
  }
}

function buscaPokemon(num) {
  let url = `https://pokeapi.co/api/v2/pokemon/${num}/`;
  let require = fetch(url);
  require.then(res =>
    res.json().then(dados => {
      console.log(dados);
      id = dados.id;
      tipos.innerHTML = ``;
      imprimirNaTela(dados.name);
      for (data of dados.stats)
        imprimirNaTela(`${data.stat.name} - ${data.base_stat}`);
      for (tipo of dados.types) imprimirNaTela(tipo.type.name);
      imprimirNaTela(`Altura: ${dados.height * 10}cm`);
      imprimirNaTela(`Peso: ${dados.weight / 10}kg`);
      imprimirNaTela(`ID: ${dados.id}`);
      img.src = dados.sprites.front_default;
    })
  );
}

function economizar(num) {
  return num != id;
}

function buscaRandomica() {
  num = Math.floor(Math.random() * 802) + 1;
  if (economizar(num)) {
    campo.value = "";
    buscaPokemon(num);
  }
}

function imprimirNaTela(info) {
  let li = document.createElement("li");
  let txt = document.createTextNode(info);
  li.appendChild(txt);
  tipos.appendChild(li);
}

campo.addEventListener("focusout", checarNumero);
sorte.addEventListener("click", buscaRandomica);
