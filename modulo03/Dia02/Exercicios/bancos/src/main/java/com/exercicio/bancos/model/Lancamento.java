package com.exercicio.bancos.model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author douglas.marques
 */
@Entity
@Table(name = "LANCAMENTO")
@NamedQueries({
    @NamedQuery(name = "Lancamento.findAll", query = "SELECT l FROM Lancamento l")
    , @NamedQuery(name = "Lancamento.findByIdlancamento", query = "SELECT l FROM Lancamento l WHERE l.idlancamento = :idlancamento")
    , @NamedQuery(name = "Lancamento.findByIdconta", query = "SELECT l FROM Lancamento l WHERE l.idconta = :idconta")
    , @NamedQuery(name = "Lancamento.findByTipolancamento", query = "SELECT l FROM Lancamento l WHERE l.tipolancamento = :tipolancamento")
    , @NamedQuery(name = "Lancamento.findByValorlancamento", query = "SELECT l FROM Lancamento l WHERE l.valorlancamento = :valorlancamento")})
public class Lancamento implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDLANCAMENTO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LANCAMENTO_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "LANCAMENTO_SEQ", sequenceName = "LANCAMENTO_SEQ")
    private Long idlancamento;
    @Basic(optional = false)
    @Column(name = "IDCONTA")
    private BigInteger idconta;
    @Basic(optional = false)
    @Column(name = "TIPOLANCAMENTO")
    private String tipolancamento;
    @Column(name = "VALORLANCAMENTO")
    private BigInteger valorlancamento;

    public Lancamento() {
    }

    public Lancamento(Long idlancamento) {
        this.idlancamento = idlancamento;
    }

    public Lancamento(Long idlancamento, BigInteger idconta, String tipolancamento) {
        this.idlancamento = idlancamento;
        this.idconta = idconta;
        this.tipolancamento = tipolancamento;
    }

    public Long getIdlancamento() {
        return idlancamento;
    }

    public void setIdlancamento(Long idlancamento) {
        this.idlancamento = idlancamento;
    }

    public BigInteger getIdconta() {
        return idconta;
    }

    public void setIdconta(BigInteger idconta) {
        this.idconta = idconta;
    }

    public String getTipolancamento() {
        return tipolancamento;
    }

    public void setTipolancamento(String tipolancamento) {
        this.tipolancamento = tipolancamento;
    }

    public BigInteger getValorlancamento() {
        return valorlancamento;
    }

    public void setValorlancamento(BigInteger valorlancamento) {
        this.valorlancamento = valorlancamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlancamento != null ? idlancamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lancamento)) {
            return false;
        }
        Lancamento other = (Lancamento) object;
        if ((this.idlancamento == null && other.idlancamento != null) || (this.idlancamento != null && !this.idlancamento.equals(other.idlancamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exercicio.bancos.model.Lancamento[ idlancamento=" + idlancamento + " ]";
    }
}
