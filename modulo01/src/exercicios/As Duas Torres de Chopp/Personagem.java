public abstract class Personagem {    
    private String nome;
    private double vida;
    private Status status;
    protected Inventario inv;    
    protected int QTD_XP;
    protected double QTD_DANO;
    
    protected Personagem(String nome, double vida, Status status) {
        this.nome = nome;
        this.vida = vida;
        this.status = status;
        this.inv = new Inventario(99);        
        this.QTD_XP = 1;
        this.QTD_DANO = 0;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void getNome(String nome) {
        this.nome = nome;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    protected void setVida(double vida) {
        this.vida = vida;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    protected void setStatus(Status status) {
        this.status = status;
    }
    
    public Inventario getInventario() {
        return this.inv;
    }
    
    public void confereVida() {
        if(this.vida <= 0) this.setStatus(Status.MORTO);        
    }
    
    public void ganharItem(Item it) {
        if(it == null) return;
        if(this.inv.buscarItem(it.getDescricao()) != null) {
            Item jaExiste = this.inv.buscarItem(it.getDescricao());
            jaExiste.setQuantidade(jaExiste.getQuantidade() + it.getQuantidade());
        } else this.inv.adicionarItem(it);
    }
    
    public void perdeItem(Item it) {
        if(it == null) return;
        this.inv.getItems().remove(it);
    }
    
    public boolean receberDano() {
        if(this.getStatus() == Status.MORTO) return false;
        this.setVida(this.getVida()-QTD_DANO);
        this.confereVida();
        return true;
    }
    
    protected void ganharVida(double vida) {
        this.setVida(this.getVida()+vida);
    }
}