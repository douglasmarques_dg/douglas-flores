package com.petshop.entity;

public abstract class Generic<ID> {
    
    public abstract ID getId();    
}
