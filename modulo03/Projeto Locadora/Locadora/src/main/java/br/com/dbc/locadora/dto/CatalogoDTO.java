package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.MidiaType;
import java.util.HashMap;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author douglas.marques
 */
@Data
@Builder
public class CatalogoDTO {
    
    private Filme filme;
    
    private String disponibilidade;
    
    private HashMap<MidiaType, Double> valores;
}
