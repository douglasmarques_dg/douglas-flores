package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.service.FilmeService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author douglas.marques
 */
@RestController
@RequestMapping("/api/filme")
public class FilmeRestController extends AbstractRestController<Filme, Long, FilmeService> {

    @Autowired
    private FilmeService filmeService;

    @Override
    protected FilmeService getService() {
        return filmeService;
    }

    @PostMapping("/midia")
    public ResponseEntity<?> post(@RequestBody FilmeDTO input) {
        if (input.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(filmeService.save(input));
    }

    @GetMapping("/buscar/categoria/{categoria}")
    public ResponseEntity<?> get(Pageable pageable, @PathVariable Categoria categoria) {
        return ResponseEntity.ok(filmeService.findByCategoria(pageable, categoria));
    }

    @GetMapping("/buscar/titulo/{titulo}")
    public ResponseEntity<?> get(Pageable pageable, @PathVariable String titulo) {
        return ResponseEntity.ok(filmeService.findByTitulo(pageable, titulo));
    }

    @GetMapping("/buscar/lancamento/{data}")
    public ResponseEntity<?> get(Pageable pageable, @DateTimeFormat(pattern = "dd-MM-yyyy") @PathVariable("data") LocalDate data) {
        return ResponseEntity.ok(filmeService.findByLancamento(pageable, data));
    }

    @PutMapping("/{id}/midia")
    public ResponseEntity<?> updateFilmeAndMidias(@PathVariable Long id, @RequestBody FilmeDTO dto) {
        if (!id.equals(dto.getId())) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(filmeService.updateFilmeAndMidia(dto));
    }

    @GetMapping("/contar/{id}/{tipo}")
    public ResponseEntity<?> get(@PathVariable Long id, @PathVariable MidiaType tipo) {
        return ResponseEntity.ok(filmeService.countByTipo(id, tipo));
    }
    
    @GetMapping("/precos/{id}")
    public ResponseEntity<?> getPrecos(@PathVariable Long id) {
        if(id == null) {
            return ResponseEntity.badRequest().build();
        } else {
            return ResponseEntity.ok(filmeService.findPrecos(id));
        }
    }
    
    @PostMapping("/buscar/catalogo")
    public ResponseEntity<?> getCatalogo(Pageable pageable, @RequestBody FilmeDTO nomeFilme) {
        return ResponseEntity.ok(filmeService.catalogoDeFilmes(pageable, nomeFilme));
    }
}
