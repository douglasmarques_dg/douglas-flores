package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import static java.time.format.DateTimeFormatter.ofPattern;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author douglas.marques
 */
public class FilmeRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Override
    protected AbstractRestController getController() {
        return filmeRestController;
    }

    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {

    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void filmeCreateTest() throws Exception {
        Filme filme = Filme.builder()
                .titulo("Rei Leão")
                .lancamento(LocalDate.of(1994, 07, 06))
                .categoria(Categoria.AVENTURA)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filme.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filme.getLancamento().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filme.getCategoria().toString()));

        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(filme.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(filme.getCategoria(), filmes.get(0).getCategoria());
        Assert.assertEquals(filme.getLancamento(), filmes.get(0).getLancamento());
    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void filmeCreateMidiaTest() throws Exception {
        Filme f = Filme.builder()
                .titulo("007 Goldeneye")
                .lancamento(LocalDate.of(1993, 04, 25))
                .categoria(Categoria.ACAO)
                .build();

        Midia m = Midia.builder()
                .tipo(MidiaType.VHS).
                idFilme(f)
                .build();

        ArrayList<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO midiaDTO = MidiaDTO.builder()
                .tipo(m.getTipo())
                .quantidade(5)
                .valor(3.2)
                .build();

        midias.add(midiaDTO);

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo(f.getTitulo())
                .lancamento(f.getLancamento())
                .categoria(f.getCategoria())
                .midia(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("1993-04-25"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value("ACAO"));

        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(f.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(f.getCategoria(), filmes.get(0).getCategoria());
        Assert.assertEquals(f.getLancamento(), filmes.get(0).getLancamento());

        List<Midia> midiasDoBanco = midiaRepository.findAll();
        Assert.assertEquals(5, midiasDoBanco.size());
        midiasDoBanco.forEach((midia) -> {
            Assert.assertEquals(midias.get(0).getTipo(), midia.getTipo());
            Assert.assertEquals(filmes.get(0), midia.getIdFilme());
            Assert.assertEquals(null, midia.getIdAluguel());
        });
    }

    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    @Test
    public void findFilmeByCategoria() throws Exception {
        Filme f = Filme.builder()
                .titulo("007 Goldeneye")
                .lancamento(LocalDate.of(1993, 04, 25))
                .categoria(Categoria.ACAO)
                .build();

        Midia m = Midia.builder()
                .tipo(MidiaType.VHS).
                idFilme(f)
                .build();

        ArrayList<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO midiaDTO = MidiaDTO.builder()
                .tipo(m.getTipo())
                .quantidade(5)
                .valor(3.2)
                .build();

        midias.add(midiaDTO);

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo(f.getTitulo())
                .lancamento(f.getLancamento())
                .categoria(f.getCategoria())
                .midia(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("1993-04-25"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value("ACAO"));

        restMockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/filme/buscar/categoria/%s", filmeDTO.getCategoria()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                //.andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].lancamento").value("1993-04-25"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].categoria").value("ACAO"));
    }

    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    @Test
    public void findFilmeByTitulo() throws Exception {
        Filme f = Filme.builder()
                .titulo("007 Goldeneye")
                .lancamento(LocalDate.of(1993, 04, 25))
                .categoria(Categoria.ACAO)
                .build();

        Midia m = Midia.builder()
                .tipo(MidiaType.VHS).
                idFilme(f)
                .build();

        ArrayList<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO midiaDTO = MidiaDTO.builder()
                .tipo(m.getTipo())
                .quantidade(5)
                .valor(3.2)
                .build();

        midias.add(midiaDTO);

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo(f.getTitulo())
                .lancamento(f.getLancamento())
                .categoria(f.getCategoria())
                .midia(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("1993-04-25"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value("ACAO"));

        restMockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/filme/buscar/titulo/%s", filmeDTO.getTitulo()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].lancamento").value("1993-04-25"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].categoria").value("ACAO"));
    }

    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    @Test
    public void findFilmeByLancamento() throws Exception {
        Filme f = Filme.builder()
                .titulo("007 Goldeneye")
                .lancamento(LocalDate.of(1993, 04, 25))
                .categoria(Categoria.ACAO)
                .build();

        Midia m = Midia.builder()
                .tipo(MidiaType.VHS).
                idFilme(f)
                .build();

        ArrayList<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO midiaDTO = MidiaDTO.builder()
                .tipo(m.getTipo())
                .quantidade(5)
                .valor(3.2)
                .build();

        midias.add(midiaDTO);

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo(f.getTitulo())
                .lancamento(f.getLancamento())
                .categoria(f.getCategoria())
                .midia(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("1993-04-25"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value("ACAO"));

        restMockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/filme/buscar/lancamento/25-04-1993"))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].lancamento").value("1993-04-25"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].categoria").value("ACAO"));
    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void updateFilmeTest() throws Exception {
        List<MidiaDTO> midiasDTO = Arrays.asList(
                MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(1.9d).build(),
                MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(1.9d).build(),
                MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(1.9d).build()
        );

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(new ArrayList<>(midiasDTO))
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)));

        Filme filmeSave = filmeRepository.findAll().get(0);

        filmeDTO.setId(filmeSave.getId());
        filmeDTO.getMidia().forEach(midiaDTO -> {
            midiaDTO.setValor(9.0d);
        });

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/" + filmeDTO.getId() + "/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("2018-10-10"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filmeDTO.getCategoria().name()));

        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(filmeDTO.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(filmeDTO.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(filmeDTO.getCategoria(), filmes.get(0).getCategoria());

        List<Midia> midias = midiaRepository.findAll();
        Assert.assertEquals(3, midias.size());
        Assert.assertEquals(midiasDTO.get(0).getTipo(), midias.get(0).getTipo());
        Assert.assertEquals(filmes.get(0), midias.get(0).getIdFilme());

        List<ValorMidia> valorMidias = valorMidiaRepository.findAll();

        Assert.assertEquals(6, valorMidias.size());

        List<ValorMidia> valorMidiasAntigos = valorMidias.stream().filter(v -> v.getFinalVigencia() != null).collect(Collectors.toList());
        Assert.assertEquals(3, valorMidiasAntigos.size());

        valorMidiasAntigos.stream().forEach(v -> {
            Assert.assertEquals(1.9d, v.getValor(), 0.01d);
            Assert.assertEquals(LocalDateTime.now().format(ofPattern("dd/MM/yyyy HH:mm")),
                    v.getInicioVigencia().format(ofPattern("dd/MM/yyyy HH:mm")));
            Assert.assertEquals(LocalDateTime.now().format(ofPattern("dd/MM/yyyy HH:mm")),
                    v.getFinalVigencia().format(ofPattern("dd/MM/yyyy HH:mm")));
        });

        List<ValorMidia> valorMidiasNovos = valorMidias.stream().filter(v -> v.getFinalVigencia() == null).collect(Collectors.toList());
        Assert.assertEquals(3, valorMidiasNovos.size());

        valorMidiasNovos.stream().forEach(v -> {
            Assert.assertEquals(9.0d, v.getValor(), 0.01d);
            Assert.assertEquals(LocalDateTime.now().format(ofPattern("dd/MM/yyyy HH:mm")),
                    v.getInicioVigencia().format(ofPattern("dd/MM/yyyy HH:mm")));
            Assert.assertNull(v.getFinalVigencia());
        });
    }

    @Test
    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    public void buscaCatalogoTest() throws Exception {
        Filme f = Filme.builder()
                .titulo("007 Goldeneye")
                .lancamento(LocalDate.of(1993, 04, 25))
                .categoria(Categoria.ACAO)
                .build();

        Midia m = Midia.builder()
                .tipo(MidiaType.VHS).
                idFilme(f)
                .build();

        ArrayList<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO midiaDTO = MidiaDTO.builder()
                .tipo(m.getTipo())
                .quantidade(5)
                .valor(3.2)
                .build();

        midias.add(midiaDTO);

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo(f.getTitulo())
                .lancamento(f.getLancamento())
                .categoria(f.getCategoria())
                .midia(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("1993-04-25"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value("ACAO"));

        FilmeDTO filmeDTOBusca = FilmeDTO.builder()
                .titulo("007")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/buscar/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTOBusca)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].filme.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].filme.lancamento").value("1993-04-25"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].filme.categoria").value("ACAO"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].disponibilidade").value("Disponivel!"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].valores.VHS").value("3.2"));
    }
}
