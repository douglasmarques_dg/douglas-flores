package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.HibernateUtil;
import org.hibernate.SessionFactory;
import br.com.dbc.petshop.entity.Animal;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author douglas.marques
 */
public class AnimalDAOHIBERNATE {

    private static AnimalDAOHIBERNATE instance;
    protected SessionFactory sessionFactory;

    public static AnimalDAOHIBERNATE getInstance() {
        if (instance == null) {
            instance = new AnimalDAOHIBERNATE();
        }
        return instance;
    }

    public AnimalDAOHIBERNATE() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public List<Animal> findAll() {
        Session session = sessionFactory.openSession();
        return session.createCriteria(Animal.class).list();
    }

    public List<Animal> findName(String nome) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            return session.createCriteria(Animal.class)
                    .add(Restrictions.ilike("nome", nome))
                    .list();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial Session creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        } finally {
            session.close();
        }
    }

    public void saveOrUpdate(Animal animal) {
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();
        try {
            session.saveOrUpdate(animal);
            t.commit();
        } catch (Exception e) {
            t.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    public void delete(Animal animal) {
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();
        try {
            session.delete(animal);
            t.commit();
        } catch (Exception e) {
            t.rollback();
            throw e;
        } finally {
            session.close();
        }
    }
}
