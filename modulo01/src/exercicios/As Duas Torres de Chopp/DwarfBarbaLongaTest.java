import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest {
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void perderVidaTest() {
        DadoD6FAKE dado = new DadoD6FAKE();
        dado.setValorFalso(1);
        DwarfBarbaLonga gimli = new DwarfBarbaLonga("Gilmli", dado);
        gimli.receberDano();
        assertEquals(110, gimli.getVida(), 0.1);
        dado.setValorFalso(2);
        gimli.receberDano();
        assertEquals(110, gimli.getVida(), 0.1);
    }
}