package com.exercicio.model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author douglas.marques
 */
@Entity
@Table(name = "ITEMNOTA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Itemnota.findAll", query = "SELECT i FROM Itemnota i")
    , @NamedQuery(name = "Itemnota.findByIditemnota", query = "SELECT i FROM Itemnota i WHERE i.iditemnota = :iditemnota")
    , @NamedQuery(name = "Itemnota.findByQuantidade", query = "SELECT i FROM Itemnota i WHERE i.quantidade = :quantidade")})
public class Itemnota implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDITEMNOTA")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ITEM_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")
    private Long iditemnota;
    @Basic(optional = false)
    @Column(name = "QUANTIDADE")
    private BigInteger quantidade;
    @JoinColumn(name = "IDNOTA", referencedColumnName = "IDNOTA")
    @ManyToOne(optional = false)
    private Nota idnota;
    @JoinColumn(name = "IDPRODUTO", referencedColumnName = "IDPRODUTO")
    @ManyToOne(optional = false)
    private Produto idproduto;

    public Itemnota() {
    }

    public Itemnota(Long iditemnota) {
        this.iditemnota = iditemnota;
    }

    public Itemnota(Long iditemnota, BigInteger quantidade) {
        this.iditemnota = iditemnota;
        this.quantidade = quantidade;
    }

    public Long getIditemnota() {
        return iditemnota;
    }

    public void setIditemnota(Long iditemnota) {
        this.iditemnota = iditemnota;
    }

    public BigInteger getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigInteger quantidade) {
        this.quantidade = quantidade;
    }

    public Nota getIdnota() {
        return idnota;
    }

    public void setIdnota(Nota idnota) {
        this.idnota = idnota;
    }

    public Produto getIdproduto() {
        return idproduto;
    }

    public void setIdproduto(Produto idproduto) {
        this.idproduto = idproduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iditemnota != null ? iditemnota.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Itemnota)) {
            return false;
        }
        Itemnota other = (Itemnota) object;
        if ((this.iditemnota == null && other.iditemnota != null) || (this.iditemnota != null && !this.iditemnota.equals(other.iditemnota))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exercicio.model.Itemnota[ iditemnota=" + iditemnota + " ]";
    }
}
