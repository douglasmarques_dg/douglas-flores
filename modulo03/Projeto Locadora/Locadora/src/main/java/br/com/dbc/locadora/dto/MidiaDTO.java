package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.MidiaType;
import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author douglas.marques
 */
@Data
@Builder
public class MidiaDTO implements Serializable {

    private MidiaType tipo;

    private Integer quantidade;

    private Double valor;
}
